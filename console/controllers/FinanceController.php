<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\behaviors\StatusBehavior;
use common\components\foreignSystems\EtherScanIO;
use common\forms\PaymentRequestForm;
use common\models\mining\MiningDaily;
use common\models\user\User;
use common\models\userPackage\UserPackage;
use common\models\financeOperation\FinanceOperation;
use common\models\userNotification\UserNotification;
use common\models\currency\Currency;
use common\models\userBalance\UserBalance;
use common\models\userBalance\UserBalanceHistory;
use common\models\payment\UserPaymentRequest;
use common\models\userPurse\UserPurse;

class FinanceController extends Controller
{
    public function actionCalculateProfit()
    {
        $date = date('Y-m-d', strtotime('-1 day'));

        $currencyList = Currency::getList();

        foreach ($currencyList as $currencyId => $currencyAlias) {

            /** @var MiningDaily $miningData */
            $miningData = MiningDaily::find()
                ->andWhere(['currency_id' => $currencyId])
                ->andWhere(['date' => $date])
                ->one();

            if (empty($miningData)) {
                continue;
            }

            /** @var UserPackage[] $userPackages */
            $userPowers = UserPackage::find()
                ->alias('t')
                ->select(['t.user_id', 'power' => 'SUM(package.power)'])
                ->joinWith(['package', 'user'])
                ->andWhere(['package.profit_currency_id' => $currencyId])
                ->andWhere(['t.status' => StatusBehavior::STATUS_ACTIVE])
                ->andWhere(['package.status' => StatusBehavior::STATUS_ACTIVE])
                ->andWhere(['user.status' => User::STATUS_ACTIVE])
                ->groupBy('t.user_id')
                ->createCommand()
                ->queryAll();

            foreach ($userPowers as $item) {
                $userId = $item['user_id'];
                $userPower = $item['power'];

                $userProfit = round(($userPower * $miningData->profit) / $miningData->power, 8);

                /** @var FinanceOperation $checkOperation */
                $checkOperation = FinanceOperation::find()
                    ->andWhere(['mining_daily_id' => $miningData->id])
                    ->andWhere(['user_id' => $userId])
                    ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
                    ->one();

                if (!empty($checkOperation)) {
                    if ($checkOperation->amount == $userProfit) {
                        continue;
                    }

                    $checkOperation->disableOperation();
                }

                FinanceOperation::createIncomeOperation(
                    $userId,
                    $currencyId,
                    $userProfit,
                    'Начисление дохода за ' . $date,
                    null,
                    $miningData->id
                );

                UserNotification::createNotification($userId, 'Начислен доход ' . $currencyAlias . ' за ' . $date);
            }
        }
    }

    /**
     * @param string|null $date
     */
    public function actionSaveUserBalanceHistory($date = null)
    {
        if (empty($date) || $date == '0000-00-00') {
            $date = date('Y-m-d');
        }

        $query = UserBalance::find()
            ->joinWith(['user'])
            ->andWhere(['user.status' => User::STATUS_ACTIVE]);

        /** @var UserBalance[] $userBalances */
        foreach ($query->batch(20) as $userBalances) {
            foreach ($userBalances as $userBalance) {
                $financeQuery = FinanceOperation::find()
                    ->andWhere(['user_id' => $userBalance->user_id])
                    ->andWhere(['currency_id' => $userBalance->currency_id])
                    ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
                    ->andWhere(['<=', 'created_at', $date . ' 23:59:59']);

                $balance = (float)$financeQuery
                    ->sum('IF(type_operation = ' . FinanceOperation::TYPE_OPERATION_INCOME . ', amount, -amount)');

                $profit = (float)$financeQuery
                    ->andWhere('mining_daily_id IS NOT NULL OR parent_operation_id IS NOT NULL')
                    ->sum('IF(type_operation = ' . FinanceOperation::TYPE_OPERATION_INCOME . ', amount, -amount)');

                UserBalanceHistory::createRecord(
                    $date, $userBalance->user_id, $userBalance->currency_id, $balance, $profit);
            }
        }
    }

    public function actionCheckOperations()
    {
        $currency = Currency::getEth();
        $transactions = EtherScanIO::getListTransactions($currency);
        if (empty($transactions)) {
            return;
        }

        $paymentRequestForm = new PaymentRequestForm();

        foreach ($transactions as $transaction) {
            $hash = $transaction['hash'];
            if (empty($hash)) {
                continue;
            }

            $checkPayment = UserPaymentRequest::findOne(['payment_hash' => $hash]);
            if (!empty($checkPayment)) {
                continue;
            }

            $value = $transaction['value'];
            $decimal = str_pad(substr($value, -18), 18, '0', STR_PAD_LEFT);
            $ceil = substr_replace($value, '', -18);
            if (empty($ceil)) {
                $ceil = '0';
            }

            $amount = (float)trim($ceil . '.' . $decimal, '0');

            $fromUserPurse = UserPurse::findOne(['currency_id' => $currency->id, 'purse' => $transaction['from']]);
            $toUserPurse = UserPurse::findOne(['currency_id' => $currency->id, 'purse' => $transaction['to']]);

            if (!empty($fromUserPurse)) {
                $inPayment = UserPaymentRequest::createRecord(
                    $fromUserPurse->id, UserPaymentRequest::TYPE_PAY_IN, $amount);

                if (!empty($inPayment)) {
                    $paymentRequestForm->createFinanceOperation($inPayment, $hash);
                }

//                echo 'payment in ' . $fromUserPurse->id . PHP_EOL;
            }

            if (!empty($toUserPurse)) {
                $outPayment = UserPaymentRequest::findOne([
                    'user_purse_id' => $toUserPurse->id,
                    'type'          => UserPaymentRequest::TYPE_PAY_OUT,
                    'amount'        => $amount
                ]);

                if (!empty($outPayment)) {
                    $paymentRequestForm->createFinanceOperation($outPayment, $hash);
                }

//                echo 'payment out ' . $toUserPurse->id . PHP_EOL;
            }
        }
    }
}
