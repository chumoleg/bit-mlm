<?php

namespace console\controllers;

use common\components\foreignSystems\EtherScanIO;
use Yii;
use yii\console\Controller;
use yii\base\Exception;
use common\models\user\User;
use common\forms\RegistrationForm;
use common\models\userTree\UserTree;

class TestController extends Controller
{
    public function actionCreateAdmin($email, $password, $first = false)
    {
        $form                 = new RegistrationForm();
        $form->email          = $email;
        $form->password       = $password;
        $form->passwordRepeat = $password;

        if ($first) {
            $form->registerFirstUser = true;

            $form->inviteCode = '123456';

        } else {
            $user = User::findOne(1);

            $form->inviteCode = $user->invite_code;
        }

        if ($user = $form->registerUser()) {
            $user->role   = User::ROLE_ADMIN;
            $user->status = User::STATUS_ACTIVE;
            $user->save(false);

            echo 'success';

        } else {
            print_r($form->errors);
            echo 'error';
        }

        try {
            // Создаем пользователю корень
            $userRoot = new UserTree(['user_id' => 1]);
            $userRoot->makeRoot();

        } catch (Exception $e) {
        }
    }

    public function actionTestRegistration()
    {
        $pass = Yii::$app->security->generateRandomString(6);

        $c = 0;
        while (true) {
            if ($c == 60) {
                break;
            }

            $users       = User::find()->all();
            $inviteCodes = \yii\helpers\ArrayHelper::getColumn($users, 'invite_code');

            $inviteCode = array_rand($inviteCodes);

            $form                 = new RegistrationForm();
            $form->email          = Yii::$app->security->generateRandomString(10) . '@mmm.ru';
            $form->password       = $pass;
            $form->passwordRepeat = $pass;
            $form->inviteCode     = $inviteCodes[$inviteCode];
            $form->createdAt      = date('Y-m-d H:i:s', rand(1514764800, time()));

            if (!$user = $form->registerUser()) {
                print_r($form->errors);
                break;
            }

            $user->status = User::STATUS_ACTIVE;
            $user->save(false);

            echo 'Invite code: ' . $form->inviteCode . PHP_EOL;

            $c++;
        }
    }

    public function actionChangeUserPassword($userId, $password)
    {
        $user = User::findOne($userId);
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->save(false);

        echo 'success';
    }

    public function actionRegenerateInviteKeys()
    {
        /** @var User[] $users */
        $users = User::find()->all();
        foreach ($users as $user) {
            $user->generateInviteCode();
            $user->save();
        }
    }
}
