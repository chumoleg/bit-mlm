<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\behaviors\StatusBehavior;
use common\models\userNotification\UserNotification;
use common\models\userPackage\UserPackage;

class PackageController extends Controller
{
    public function actionCheckContractDates()
    {
        /** @var UserPackage[] $userPackages */
        $userPackages = UserPackage::find()
            ->andWhere(['<=', 'contract_last_date', date('Y-m-d')])
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
            ->all();

        foreach ($userPackages as $userPackage) {
            $userPackage->setNotActive()->save(false);

            UserNotification::createNotification(
                $userPackage->user_id,
                'Истек срок контракта. Пакет "' . $userPackage->package->name . '"'
            );
        }
    }
}
