<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'crontask'],
    'controllerNamespace' => 'console\controllers',
    'modules'             => [
        'crontask' => [
            'class'    => 'gofmanaa\crontask\Module',
            'fileName' => 'cron.txt',
            'tasks'    => [
                'calculateProfit'        => [
                    'command' => 'finance/calculate-profit',
                    'hour'    => '0',
                    'min'     => '5',
                ],
                'saveBalanceHistory'     => [
                    'command' => 'finance/save-user-balance-history',
                    'hour'    => '*/6',
                    'min'     => '15',
                ],
                'checkUserPackage'       => [
                    'command' => 'package/check-contract-dates',
                    'hour'    => '0',
                    'min'     => '30',
                ],
                'checkPaymentOperations' => [
                    'command' => 'finance/check-operations',
                    'min'     => '*/30',
                ]
            ],
        ],
    ],
    'controllerMap'       => [
        'migrate' => [
            'class'        => 'console\components\migration\MigrateController',
            'templateFile' => '@console/templates/migration/templateView.php',
        ],
        'fixture' => [
            'class'     => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
    ],
    'components'          => [
    ],
    'params'              => $params,
];
