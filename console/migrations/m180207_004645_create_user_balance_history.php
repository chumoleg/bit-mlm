<?php

use console\components\migration\Migration;

class m180207_004645_create_user_balance_history extends Migration
{
    public function up()
    {
        $this->createTable('user_balance_history', [
            'id'          => self::PRIMARY_KEY,
            'user_id'     => self::INT_FIELD_NOT_NULL,
            'date'        => 'DATE NOT NULL',
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'balance'     => 'DECIMAL(14,8) NOT NULL',
            'profit'      => 'DECIMAL(14,8) NOT NULL',
            'created_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_balance_history_user_id', 'user_balance_history', 'user_id',
            'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_balance_history_currency_id', 'user_balance_history', 'currency_id',
            'currency', 'id', 'RESTRICT', 'CASCADE');

        $this->_migrateBalances();
    }

    private function _migrateBalances()
    {
        $dateFrom = Yii::$app->db->createCommand('SELECT DATE(MIN(created_at)) FROM finance_operation')->queryScalar();
        if (empty($dateFrom)) {
            $dateFrom = date('Y-m-d');
        }

        $date = $dateFrom;

        $array = [];
        while (true) {
            $dateTo = date('Y-m-d');
            if ($date > $dateTo) {
                break;
            }

            $array[] = $date;

            $date = date('Y-m-d', strtotime($date . ' + 1 day'));
        }

        $userIds     = Yii::$app->db->createCommand('SELECT id FROM user')->queryColumn();
        $currencyIds = Yii::$app->db->createCommand('SELECT id FROM currency')->queryColumn();

        foreach ($array as $item) {
            foreach ($userIds as $userId) {
                foreach ($currencyIds as $currencyId) {
                    (new \common\models\userBalance\UserBalance())->recalculate($userId, $currencyId, $item);
                }
            }
        }
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_balance_history_user_id', 'user_balance_history');
        $this->dropForeignKey('fk_user_balance_history_currency_id', 'user_balance_history');
        $this->dropTable('user_balance_history');
    }
}
