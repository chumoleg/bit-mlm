<?php

use console\components\migration\Migration;
use common\models\currency\Currency;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->_createUser();
        $this->_createCurrency();

        $this->_createMiningDaily();

        $this->_createPackage();
        $this->_createPackagePercent();

        $this->_createUserTree();
        $this->_createUserNotification();
        $this->_createUserBalance();
        $this->_createUserPackage();
        $this->_createUserPaymentRequest();

        $this->_createFinanceOperation();
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_payment_request_user_id', 'user_payment_request');
        $this->dropForeignKey('fk_user_payment_request_currency_id', 'user_payment_request');
        $this->dropTable('user_payment_request');

        $this->dropForeignKey('fk_finance_operation_mining_daily_id', 'finance_operation');
        $this->dropForeignKey('fk_finance_operation_user_id', 'finance_operation');
        $this->dropForeignKey('fk_finance_operation_parent_operation_id', 'finance_operation');
        $this->dropForeignKey('fk_finance_operation_currency_id', 'finance_operation');
        $this->dropTable('finance_operation');

        $this->dropForeignKey('fk_user_balance_user_id', 'user_balance');
        $this->dropForeignKey('fk_user_balance_currency_id', 'user_balance');
        $this->dropTable('user_balance');

        $this->dropForeignKey('fk_user_notification_user_id', 'user_notification');
        $this->dropTable('user_notification');

        $this->dropForeignKey('fk_user_package_user_id', 'user_package');
        $this->dropForeignKey('fk_user_package_package_id', 'user_package');
        $this->dropTable('user_package');

        $this->dropForeignKey('fk_user_tree_user_id', 'user_tree');
        $this->dropTable('user_tree');

        $this->dropForeignKey('fk_package_percent_package_id', 'package_percent');
        $this->dropTable('package_percent');

        $this->dropForeignKey('fk_mining_daily_currency_id', 'mining_daily');
        $this->dropTable('mining_daily');

        $this->dropTable('package');

        $this->dropTable('currency');

        $this->dropTable('user');
    }

    private function _createUser()
    {
        $this->createTable('user', [
            'id'                   => self::PRIMARY_KEY,
            'email'                => $this->string()->notNull()->unique(),
            'invite_code'          => $this->string(9)->notNull(),
            'auth_key'             => $this->string(32)->notNull(),
            'password_hash'        => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email_confirm_token'  => $this->string(32)->unique(),
            'status'               => self::TINYINT_FIELD . ' DEFAULT 1',
            'role'                 => self::TINYINT_FIELD . ' DEFAULT 1',
            'count_visit'          => self::TINYINT_FIELD . ' DEFAULT 0',
            'last_visit'           => self::TIMESTAMP_FIELD,
            'created_at'           => self::TIMESTAMP_FIELD,
            'updated_at'           => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);
    }

    private function _createCurrency()
    {
        $this->createTable('currency', [
            'id'         => self::PRIMARY_KEY,
            'alias'      => 'VARCHAR(50) NOT NULL',
            'name'       => 'VARCHAR(50) NOT NULL',
            'power_type' => self::TINYINT_FIELD,
            'created_at' => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $array = [
            [
                'alias'      => 'BTC',
                'name'       => 'Bitcoin',
                'power_type' => Currency::POWER_TYPE_TH,
            ],
            [
                'alias'      => 'ETH',
                'name'       => 'Ethereum',
                'power_type' => Currency::POWER_TYPE_MH,
            ],
        ];

        foreach ($array as $item) {
            $this->insert('currency', array_merge($item, [
                'created_at' => date('Y-m-d H:i:s'),
            ]));
        }
    }

    private function _createMiningDaily()
    {
        $this->createTable('mining_daily', [
            'id'          => self::PRIMARY_KEY,
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'power'       => 'DECIMAL(14,2) UNSIGNED NOT NULL',
            'profit'      => 'DECIMAL(14,8) UNSIGNED NOT NULL',
            'date'        => 'DATE NOT NULL',
            'created_at'  => self::TIMESTAMP_FIELD,
            'updated_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_mining_daily_currency_id', 'mining_daily', 'currency_id', 'currency', 'id',
            'RESTRICT', 'CASCADE');
    }

    private function _createPackage()
    {
        $this->createTable('package', [
            'id'          => self::PRIMARY_KEY,
            'name'        => self::VARCHAR_FIELD,
            'description' => 'TEXT NOT NULL',
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'power'       => self::INT_FIELD_NOT_NULL,
            'price'       => 'DECIMAL(14,8) UNSIGNED NOT NULL',
            'img_src'     => 'VARCHAR(200) DEFAULT NULL',
            'is_hit'      => self::TINYINT_FIELD,
            'status'      => self::TINYINT_FIELD,
            'created_at'  => self::TIMESTAMP_FIELD,
            'updated_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_package_currency_id', 'package', 'currency_id', 'currency', 'id', 'RESTRICT',
            'RESTRICT');
    }

    private function _createPackagePercent()
    {
        $this->createTable('package_percent', [
            'id'         => self::PRIMARY_KEY,
            'package_id' => self::INT_FIELD_NOT_NULL,
            'level'      => self::TINYINT_FIELD,
            'percent'    => 'DECIMAL(5,2) UNSIGNED NOT NULL',
            'created_at' => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_package_percent_package_id', 'package_percent', 'package_id', 'package', 'id',
            'CASCADE', 'CASCADE');
    }

    private function _createUserTree()
    {
        $this->createTable('user_tree', [
            'id'         => self::PRIMARY_KEY,
            'user_id'    => self::INT_FIELD_NOT_NULL,
            'lft'        => self::INT_FIELD_NOT_NULL,
            'rgt'        => self::INT_FIELD_NOT_NULL,
            'level'      => self::TINYINT_FIELD,
            'created_at' => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->createIndex('index_lft', 'user_tree', ['lft', 'rgt']);
        $this->createIndex('index_rgt', 'user_tree', ['rgt']);

        $this->addForeignKey('fk_user_tree_user_id', 'user_tree', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    private function _createUserNotification()
    {
        $this->createTable('user_notification', [
            'id'         => self::PRIMARY_KEY,
            'user_id'    => self::INT_FIELD_NOT_NULL,
            'status'     => self::TINYINT_FIELD,
            'message'    => 'TEXT NOT NULL',
            'created_at' => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_notification_user_id', 'user_notification', 'user_id', 'user', 'id', 'CASCADE',
            'CASCADE');
    }

    private function _createUserBalance()
    {
        $this->createTable('user_balance', [
            'id'          => self::PRIMARY_KEY,
            'user_id'     => self::INT_FIELD_NOT_NULL,
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'balance'     => 'DECIMAL(14,8) DEFAULT 0',
            'created_at'  => self::TIMESTAMP_FIELD,
            'updated_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_balance_user_id', 'user_balance', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_balance_currency_id', 'user_balance', 'currency_id', 'currency', 'id', 'RESTRICT',
            'RESTRICT');
    }

    private function _createUserPackage()
    {
        $this->createTable('user_package', [
            'id'         => self::PRIMARY_KEY,
            'user_id'    => self::INT_FIELD_NOT_NULL,
            'package_id' => self::INT_FIELD_NOT_NULL,
            'price'      => 'DECIMAL(14,8) UNSIGNED NOT NULL',
            'quantity'   => 'TINYINT UNSIGNED DEFAULT 1',
            'status'     => self::TINYINT_FIELD,
            'created_at' => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_package_user_id', 'user_package', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_package_package_id', 'user_package', 'package_id', 'package', 'id', 'CASCADE',
            'CASCADE');
    }

    private function _createUserPaymentRequest()
    {
        $this->createTable('user_payment_request', [
            'id'          => self::PRIMARY_KEY,
            'user_id'     => self::INT_FIELD_NOT_NULL,
            'type'        => self::TINYINT_FIELD,
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'amount'      => 'DECIMAL(14,8) UNSIGNED NOT NULL',
            'status'      => self::TINYINT_FIELD,
            'created_at'  => self::TIMESTAMP_FIELD,
            'updated_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_payment_request_user_id', 'user_payment_request', 'user_id',
            'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_payment_request_currency_id', 'user_payment_request', 'currency_id',
            'currency', 'id', 'RESTRICT', 'CASCADE');
    }

    private function _createFinanceOperation()
    {
        $this->createTable('finance_operation', [
            'id'                  => self::PRIMARY_KEY,
            'user_id'             => self::INT_FIELD_NOT_NULL,
            'parent_operation_id' => self::INT_FIELD,
            'mining_daily_id'     => self::INT_FIELD,
            'currency_id'         => self::INT_FIELD_NOT_NULL,
            'type_operation'      => self::TINYINT_FIELD,
            'status'              => self::TINYINT_FIELD,
            'amount'              => 'DECIMAL(14,8) UNSIGNED NOT NULL',
            'comment'             => 'TEXT NOT NULL',
            'created_at'          => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_finance_operation_user_id', 'finance_operation', 'user_id',
            'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_finance_operation_parent_operation_id', 'finance_operation', 'parent_operation_id',
            'finance_operation', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk_finance_operation_currency_id', 'finance_operation', 'currency_id',
            'currency', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk_finance_operation_mining_daily_id', 'finance_operation', 'mining_daily_id',
            'mining_daily', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('index_user_status', 'finance_operation', 'user_id, status');
    }
}
