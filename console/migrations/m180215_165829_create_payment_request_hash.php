<?php

use console\components\migration\Migration;

class m180215_165829_create_payment_request_hash extends Migration
{
    public function up()
    {
        $this->addColumn('user_payment_request', 'payment_hash', 'VARCHAR(255) DEFAULT NULL AFTER type');
    }

    public function down()
    {
        $this->dropColumn('user_payment_request', 'payment_hash');
    }
}
