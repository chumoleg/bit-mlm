<?php

use console\components\migration\Migration;

class m180213_153330_create_user_purse extends Migration
{
    protected $_ignoreError = true;

    public function up()
    {
        $this->createTable('user_purse', [
            'id'          => self::PRIMARY_KEY,
            'user_id'     => self::INT_FIELD_NOT_NULL,
            'currency_id' => self::INT_FIELD_NOT_NULL,
            'purse'       => 'VARCHAR(255) NOT NULL',
            'status'      => self::TINYINT_FIELD,
            'created_at'  => self::TIMESTAMP_FIELD,
            'updated_at'  => self::TIMESTAMP_FIELD,
        ], self::TABLE_OPTIONS);

        $this->addForeignKey('fk_user_purse_user_id', 'user_purse', 'user_id',
            'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_purse_currency_id', 'user_purse', 'currency_id',
            'currency', 'id', 'RESTRICT', 'CASCADE');

        $this->delete('user_payment_request');

        $this->addColumn('user_payment_request', 'user_purse_id', self::INT_FIELD_NOT_NULL . ' AFTER id');
        $this->addForeignKey('fk_user_payment_request_user_purse_id', 'user_payment_request', 'user_purse_id',
            'user_purse', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey('fk_user_payment_request_user_id', 'user_payment_request');
        $this->dropColumn('user_payment_request', 'user_id');
        $this->dropForeignKey('fk_user_payment_request_currency_id', 'user_payment_request');
        $this->dropColumn('user_payment_request', 'currency_id');

        $this->addColumn('currency', 'receiving_purse', 'VARCHAR(255) NOT NULL AFTER power_type');

        $this->alterColumn('package', 'currency_id', self::INT_FIELD_NOT_NULL . ' AFTER price');
        $this->addColumn('package', 'profit_currency_id', self::INT_FIELD_NOT_NULL . ' AFTER power');
        $this->update('package', ['profit_currency_id' => 1]);

        $this->addForeignKey('fk_package_profit_currency_id', 'package', 'profit_currency_id',
            'currency', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_payment_request_user_purse_id', 'user_payment_request');
        $this->dropColumn('user_payment_request', 'user_purse_id');

        $this->dropForeignKey('fk_package_profit_currency_id', 'package');
        $this->dropColumn('package', 'profit_currency_id');

        $this->dropColumn('currency', 'receiving_purse');

        $this->dropForeignKey('fk_user_purse_currency_id', 'user_purse');
        $this->dropForeignKey('fk_user_purse_user_id', 'user_purse');
        $this->dropTable('user_purse');
    }
}
