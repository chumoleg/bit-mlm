<?php

use console\components\migration\Migration;

class m180128_170149_create_package_contract_days extends Migration
{
    protected $_ignoreError = true;

    public function up()
    {
        $this->addColumn('package', 'contract_days', self::INT_FIELD_NOT_NULL . ' AFTER is_hit');
        $this->addColumn('user_package', 'contract_last_date', 'DATE NULL AFTER created_at');

        $this->dropColumn('user_package', 'quantity');
    }

    public function down()
    {
        $this->dropColumn('package', 'contract_days');
        $this->dropColumn('user_package', 'contract_last_date');
    }
}
