<?php

use console\components\migration\Migration;

class m180129_011253_add_user_package_currency extends Migration
{
    public function up()
    {
        $this->addColumn('user_package', 'currency_id', self::INT_FIELD_NOT_NULL . ' AFTER price');
        $this->execute('UPDATE user_package t SET t.currency_id = (SELECT currency_id FROM package WHERE id = t.package_id)');

        $this->addForeignKey('fk_user_package_currency_id', 'user_package', 'currency_id',
            'currency', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_package_currency_id', 'user_package');
        $this->dropColumn('user_package', 'currency_id');
    }
}
