<?php

namespace backend\controllers;

use Yii;
use common\models\payment\UserPaymentRequest;
use common\models\payment\UserPaymentRequestSearch;
use common\forms\PaymentRequestForm;
use backend\components\BaseController;

class PaymentController extends BaseController
{
    public $layout = 'payment';

    public function actionNew()
    {
        return $this->_renderIndex([UserPaymentRequest::STATUS_NEW], 'Новые запросы');
    }

    /**
     * @param array  $statuses
     * @param string $title
     *
     * @return string
     */
    private function _renderIndex($statuses, $title)
    {
        $searchModel = new UserPaymentRequestSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $statuses);

        return $this->render('index', [
            'title'        => $title,
            'statuses'     => $statuses,
            'model'        => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionWait()
    {
        return $this->_renderIndex([UserPaymentRequest::STATUS_WAIT], 'Ожидают оплаты');
    }

    public function actionCompleted()
    {
        return $this->_renderIndex([UserPaymentRequest::STATUS_CANCELLED, UserPaymentRequest::STATUS_PAID],
            'Обработанные запросы');
    }

    public function actionSetCancelled($id)
    {
        $model = new PaymentRequestForm();
        $model->setCancelled($id);

        return $this->redirect(Yii::$app->request->getReferrer());
    }

    public function actionSetWait($id)
    {
        $model = new PaymentRequestForm();
        $model->setStatusWait($id);

        return $this->redirect(Yii::$app->request->getReferrer());
    }
}