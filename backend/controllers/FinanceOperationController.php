<?php

namespace backend\controllers;

use Yii;
use yii\web\ForbiddenHttpException;
use common\models\financeOperation\FinanceOperationSearch;
use backend\components\CrudController;
use backend\forms\FinanceOperationForm;

class FinanceOperationController extends CrudController
{
    protected function _getFormClassName()
    {
        return FinanceOperationForm::className();
    }

    protected function _getSearchClassName()
    {
        return FinanceOperationSearch::className();
    }

    public function actionUpdate($id)
    {
        return $this->redirect($this->getIndexUrl());
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        /** @var FinanceOperationForm $model */
        $model = $this->_loadModel($id);
        if (empty($model) || $model->isNotActive()) {
            throw new ForbiddenHttpException('Операция не найдена или уже удалена');
        }

        $model->disableOperation();

        return $this->redirect($this->getIndexUrl());
    }
}