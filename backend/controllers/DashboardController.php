<?php

namespace backend\controllers;

use Yii;
use backend\components\BaseController;

class DashboardController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}