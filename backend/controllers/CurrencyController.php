<?php

namespace backend\controllers;

use Yii;
use common\models\currency\CurrencySearch;
use backend\components\CrudController;
use backend\forms\CurrencyForm;

class CurrencyController extends CrudController
{
    protected function _getFormClassName()
    {
        return CurrencyForm::className();
    }

    protected function _getSearchClassName()
    {
        return CurrencySearch::className();
    }
}