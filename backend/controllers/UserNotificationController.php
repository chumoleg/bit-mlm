<?php

namespace backend\controllers;

use Yii;
use common\models\userNotification\UserNotificationSearch;
use backend\components\CrudController;
use backend\forms\UserNotificationForm;

class UserNotificationController extends CrudController
{
    protected function _getFormClassName()
    {
        return UserNotificationForm::className();
    }

    protected function _getSearchClassName()
    {
        return UserNotificationSearch::className();
    }
}