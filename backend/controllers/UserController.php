<?php

namespace backend\controllers;

use Yii;
use common\components\UrlHelper;
use common\models\user\User;
use common\models\user\UserSearch;
use backend\components\CrudController;
use backend\forms\UserForm;

class UserController extends CrudController
{
    protected function _getFormClassName()
    {
        return UserForm::className();
    }

    protected function _getSearchClassName()
    {
        return UserSearch::className();
    }

    public function actionSwitchIdentity($id)
    {
        $model = User::findOne($id);

        if (!$model->getAuthKey()) {
            $model->generateAuthKey();
            $model->save();
        }

        $url = UrlHelper::getFrontendHost() . '/auth/switch-identity?authKey=' . $model->getAuthKey();

        return $this->redirect($url);
    }
}