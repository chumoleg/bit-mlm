<?php

namespace backend\controllers;

use Yii;
use yii\web\ForbiddenHttpException;
use common\models\userPackage\UserPackageSearch;
use common\models\userPackage\UserPackage;
use backend\components\BaseController;

class UserPackageController extends BaseController
{
    public function actionIndex()
    {
        $searchModel  = new UserPackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = UserPackage::findOne($id);
        if (empty($model) || $model->isNotActive()) {
            throw new ForbiddenHttpException('Контракт не найден или уже закрыт');
        }

        $model->setNotActive()->save(false);

        return $this->redirect($this->getIndexUrl());
    }
}