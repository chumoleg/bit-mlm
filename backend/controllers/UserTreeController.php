<?php

namespace backend\controllers;

use Yii;
use common\models\userTree\UserTreeSearch;
use backend\components\BaseController;

class UserTreeController extends BaseController
{
    public function actionIndex()
    {
        $searchModel  = new UserTreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}