<?php

namespace backend\controllers;

use Yii;
use common\models\package\PackageSearch;
use backend\forms\PackageForm;
use backend\components\CrudController;
use yii\web\UploadedFile;

class PackageController extends CrudController
{
    protected function _getFormClassName()
    {
        return PackageForm::className();
    }

    protected function _getSearchClassName()
    {
        return PackageSearch::className();
    }

    protected function _beforeValidateModel($model)
    {
        $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
    }

    /**
     * @param int $id
     *
     * @return null|PackageForm
     */
    protected function _loadModel($id)
    {
        /** @var PackageForm $model */
        $model = parent::_loadModel($id);

        $model->percentData = $model->getPercentArray();

        return $model;
    }
}