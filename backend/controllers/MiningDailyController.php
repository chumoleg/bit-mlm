<?php

namespace backend\controllers;

use Yii;
use yii\web\ForbiddenHttpException;
use common\models\mining\MiningDailySearch;
use backend\components\CrudController;
use backend\forms\MiningDailyForm;

class MiningDailyController extends CrudController
{
    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->_loadModel($id);
        if ($model->hasFinanceOperations()) {
            throw new ForbiddenHttpException('Невозможно удалить, т.к. уже привязаны финаносвые операции');
        }

        $model->delete();

        return $this->redirect($this->getIndexUrl());
    }

    protected function _getFormClassName()
    {
        return MiningDailyForm::className();
    }

    protected function _getSearchClassName()
    {
        return MiningDailySearch::className();
    }
}