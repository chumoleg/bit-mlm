<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use common\models\payment\UserPaymentRequest;

$this->title = $title;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $model,
    'columns'      => [
        [
            'attribute'      => 'id',
            'contentOptions' => ['width' => '100'],
        ],
        [
            'attribute' => 'userEmail',
            'value'     => 'userPurse.user.email',
        ],
        [
            'class'     => \common\components\grid\AmountColumn::className(),
            'attribute' => 'amount',
        ],
        [
            'attribute' => 'currencyId',
            'filter'    => \common\models\currency\Currency::getList(),
            'value'     => 'userPurse.currency.alias'
        ],
        [
            'attribute'      => 'purse',
            'value'          => 'userPurse.purse',
            'contentOptions' => ['class' => 'wrappedColumn'],
        ],
        [
            'class'   => \common\components\grid\StatusColumn::className(),
            'visible' => in_array(UserPaymentRequest::STATUS_PAID, $statuses),
        ],
        [
            'attribute'      => 'payment_hash',
            'visible'        => in_array(UserPaymentRequest::STATUS_PAID, $statuses),
            'contentOptions' => ['class' => 'wrappedColumn'],
        ],
        [
            'class' => \common\components\grid\DateColumn::className(),
        ],
        [
            'class'     => \common\components\grid\DateColumn::className(),
            'attribute' => 'updated_at',
        ],
        [
            'class'    => 'yii\grid\ActionColumn',
            'template' => '{cancel} {wait}',
            'visible'  => in_array(UserPaymentRequest::STATUS_NEW, $statuses),
            'buttons'  => [
                'cancel' => function ($url, $model) {
                    $url = \yii\helpers\Url::to(['set-cancelled', 'id' => $model->id]);

                    return Html::a(Html::icon('remove'), $url, [
                        'title' => 'Отклонить запрос',
                        'class' => 'btn btn-sm btn-default',
                    ]);
                },
                'wait'   => function ($url, $model) {
                    if ($model->type != UserPaymentRequest::TYPE_PAY_OUT) {
                        return null;
                    }

                    $url = \yii\helpers\Url::to(['set-wait', 'id' => $model->id]);

                    return Html::a(Html::icon('thumbs-up'), $url, [
                        'title' => 'Пометить как перечислено',
                        'class' => 'btn btn-sm btn-success',
                    ]);
                },
            ],
        ],
    ],
]);