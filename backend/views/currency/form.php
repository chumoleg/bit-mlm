<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Создание/изменение типа баланса';

$this->params['breadcrumbs'][] = ['label' => 'Список типов балансов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый тип баланса' : $model->name;

?>

<?php
$form = ActiveForm::begin(['enableClientValidation' => false]);
?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'alias')->textInput() ?>
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'power_type')->dropDownList(\common\models\currency\Currency::$powerTypeList) ?>
        <?= $form->field($model, 'receiving_purse')->textInput() ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
