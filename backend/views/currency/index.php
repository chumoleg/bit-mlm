<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

$this->title = 'Управление валютами';

$this->context->topButtons[] = Html::a(Html::icon('plus') . ' Добавить новую валюту', ['create'],
    ['class' => 'c-btn c-btn--info']);
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            'alias',
            'name',
            'receiving_purse',
            [
                'attribute' => 'power_type',
                'filter'    => \common\models\currency\Currency::$powerTypeList,
                'value'     => function ($data) {
                    return $data->getPowerTypeLabel();
                },
            ],
            [
                'attribute' => 'sumAmount',
                'filter'    => false,
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'options'  => ['width' => '80'],
                'buttons'  => [
                    'update' => function ($url, $model) {

                        $url = \yii\helpers\Url::to(['currency/update', 'id' => $model->id]);

                        return Html::a(Html::icon('pencil'), $url, [
                            'title' => 'Редактировать',
                            'class' => 'btn btn-sm btn-default',
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
