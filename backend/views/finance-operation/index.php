<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;

$this->title = 'Финансовые операции';

$this->context->topButtons[] = Html::a(Html::icon('plus') . ' Создать финансовую операцию', ['create'],
    ['class' => 'c-btn c-btn--info']);
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'rowOptions'   => function ($model, $index, $widget, $grid) {
            $cssClass = 'u-color-success';
            if ($model->type_operation == \common\models\financeOperation\FinanceOperation::TYPE_OPERATION_WRITTEN) {
                $cssClass = 'u-color-danger';
            }

            return ['class' => $cssClass];
        },
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            [
                'attribute' => 'userEmail',
                'value'     => 'user.email',
            ],
            [
                'class' => \common\components\grid\CurrencyColumn::className(),
            ],
            [
                'attribute' => 'type_operation',
                'filter'    => \common\models\financeOperation\FinanceOperation::$typeOperationList,
                'value'     => function ($data) {
                    return $data->getTypeOperationLabel();
                },
            ],
            //            [
            //                'class'     => \common\components\grid\YesNoColumn::className(),
            //                'attribute' => 'mining_daily_id',
            //            ],
            [
                'attribute' => 'amount',
                'value'     => function ($data) {
                    return $data->getAmountLabel();
                },
            ],
            [
                'attribute'      => 'comment',
                'contentOptions' => [
                    'class' => 'wrappedColumn'
                ]
            ],
            [
                'class' => \common\components\grid\StatusColumn::className(),
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'options'  => ['width' => '80'],
                'buttons'  => [
                    'delete' => function ($url, $model) {
                        if ($model->isNotActive()) {
                            return null;
                        }

                        $url = \yii\helpers\Url::to(['delete', 'id' => $model->id]);

                        return Html::a(Html::icon('remove'), $url, [
                            'title'        => 'Удалить',
                            'class'        => 'btn btn-sm btn-default',
                            'data-confirm' => 'Вы уверены, что хотите удалить эти данные?',
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
