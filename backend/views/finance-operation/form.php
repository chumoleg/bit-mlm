<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Создание финансовой операции';

$this->params['breadcrumbs'][] = ['label' => 'Список финансовых операций', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Новая финансовая операция';

?>

<?php
$form = ActiveForm::begin(['enableClientValidation' => false]);
?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'userEmail')->textInput() ?>
        <?= $form->field($model, 'type_operation')
            ->dropDownList(\common\models\financeOperation\FinanceOperation::$typeOperationList) ?>
        <?= $form->field($model, 'amount')->textInput() ?>
        <?= $form->field($model, 'currency_id')
            ->dropDownList(\common\models\currency\Currency::getList()) ?>
        <?= $form->field($model, 'comment')->textInput() ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Провести операцию', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
