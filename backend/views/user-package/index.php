<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;

$this->title = 'Контракты пользователей';
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            [
                'attribute' => 'userEmail',
                'value'     => 'user.email',
            ],
            [
                'attribute' => 'packageName',
                'value'     => 'package.name',
            ],
            [
                'attribute' => 'price',
                'format'    => ['decimal', 8],
            ],
            [
                'class' => \common\components\grid\CurrencyColumn::className(),
            ],
            [
                'class' => \common\components\grid\StatusColumn::className(),
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
            [
                'class'     => \common\components\grid\DateColumn::className(),
                'attribute' => 'contract_last_date',
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'options'  => ['width' => '80'],
                'buttons'  => [
                    'delete' => function ($url, $model) {
                        if ($model->isNotActive()) {
                            return null;
                        }

                        $url = \yii\helpers\Url::to(['delete', 'id' => $model->id]);

                        return Html::a(Html::icon('remove'), $url, [
                            'title'        => 'Закрыть контракт',
                            'class'        => 'btn btn-sm btn-default',
                            'data-confirm' => 'Вы уверены, что хотите закрыть контракт?',
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
