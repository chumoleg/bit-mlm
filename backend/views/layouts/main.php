<?php

use backend\components\MainMenuHelper;

?>

<?php $this->beginContent('@app/views/layouts/blank.php'); ?>

    <div class="o-page__sidebar js-page-sidebar">
        <div class="c-sidebar c-sidebar--light">
            <a class="c-sidebar__brand" href="/">
                <img class="c-sidebar__brand-img" src="/static/img/Resimico_Ru_Black.png" alt="Logo"> Админка
            </a>

            <?php
            $requestUrl = Yii::$app->request->url;

            $newPaymentRequestBadge = MainMenuHelper::getPaymentRequestBadge();

            $menuItems = [
                [
                    'label' => 'Финансы',
                    'items' => [
                        [
                            'label'  => 'Аналитика',
                            'url'    => '/dashboard/index',
                            'active' => (bool)strstr($requestUrl, '/dashboard/') || $requestUrl == '/',
                            'class'  => 'fa fa-line-chart u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Финансовые операции',
                            'url'    => '/finance-operation/index',
                            'active' => (bool)strstr($requestUrl, '/finance-operation/'),
                            'class'  => 'fa fa-money u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Запросы на вывод средств' . $newPaymentRequestBadge,
                            'url'    => '/payment/new',
                            'active' => (bool)strstr($requestUrl, '/payment/'),
                            'class'  => 'fa fa-ticket u-mr-xsmall',
                        ],
                    ],
                ],
                [
                    'label' => 'Управление',
                    'items' => [
                        [
                            'label'  => 'Валюты',
                            'url'    => '/currency/index',
                            'active' => (bool)strstr($requestUrl, '/currency/'),
                            'class'  => 'fa fa-desktop u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Пакеты мощностей',
                            'url'    => '/package/index',
                            'active' => (bool)strstr($requestUrl, '/package/'),
                            'class'  => 'fa fa-table u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Текущие мощности майнеров',
                            'url'    => '/mining-daily/index',
                            'active' => (bool)strstr($requestUrl, '/mining-daily/'),
                            'class'  => 'fa fa-line-chart u-mr-xsmall',
                        ],
                    ],
                ],
                [
                    'label' => 'Пользователи',
                    'items' => [
                        [
                            'label'  => 'Список',
                            'url'    => '/user/index',
                            'active' => (bool)strstr($requestUrl, '/user/'),
                            'class'  => 'fa fa-user u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Контракты',
                            'url'    => '/user-package/index',
                            'active' => (bool)strstr($requestUrl, '/user-package/'),
                            'class'  => 'fa fa-line-chart u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Уведомления',
                            'url'    => '/user-notification/index',
                            'active' => (bool)strstr($requestUrl, '/user-notification/'),
                            'class'  => 'fa fa-forumbee u-mr-xsmall',
                        ],
                        [
                            'label'  => 'Дерево',
                            'url'    => '/user-tree/index',
                            'active' => (bool)strstr($requestUrl, '/user-tree/'),
                            'class'  => 'fa fa-tree u-mr-xsmall',
                        ],
                    ],
                ],

                [
                    'label' => '',
                    'items' => [
                        [
                            'label'  => 'Выход',
                            'url'    => '/site/logout',
                            'active' => false,
                            'class'  => 'fa fa-sign-out u-mr-xsmall',
                        ],
                    ],
                ],
            ];
            ?>

            <?php foreach ($menuItems as $menuItem) : ?>
                <h4 class="c-sidebar__title"><?= $menuItem['label']; ?></h4>

                <ul class="c-sidebar__list">
                    <?php foreach ($menuItem['items'] as $item) : ?>
                        <li class="c-sidebar__item">
                            <a class="c-sidebar__link <?= $item['active'] ? 'is-active' : ''; ?>"
                               href="<?= $item['url']; ?>">
                                <i class="<?= $item['class']; ?>"></i><?= $item['label']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

            <?php endforeach; ?>
        </div>
    </div>

    <main class="o-page__content">
        <header class="c-navbar u-mb-medium">
            <button class="c-sidebar-toggle u-mr-small">
                <span class="c-sidebar-toggle__bar"></span>
                <span class="c-sidebar-toggle__bar"></span>
                <span class="c-sidebar-toggle__bar"></span>
            </button><!-- // .c-sidebar-toggle -->

            <h2 class="c-navbar__title u-mr-auto"><?= $this->title; ?></h2>

            <div class="row">
                <div class="col-md-12 text-right">
                    <?php
                    foreach ($this->context->topButtons as $topButton) {
                        echo $topButton;
                    }
                    ?>
                </div>
            </div>

        </header>

        <div class="container-fluid">
            <?= \yii\widgets\Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>

            <?= \common\widgets\Alert::widget(); ?>

            <?= $content; ?>
        </div>
    </main>

<?php $this->endContent(); ?>