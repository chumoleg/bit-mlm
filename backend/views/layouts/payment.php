
<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="c-tabs">
<?= \yii\bootstrap\Tabs::widget(
    [
        'id'    => 'paymentTabs',
        'items' => [
            [
                'label'  => 'Новые',
                'url'    => ['payment/new'],
                'linkOptions' => [
                    'class' => strstr(Yii::$app->request->url, 'payment/new') ? 'c-tabs__link active' : 'c-tabs__link'
                ]
            ],
            [
                'label'  => 'На проверке',
                'url'    => ['payment/wait'],
                'linkOptions' => [
                    'class' => strstr(Yii::$app->request->url, 'payment/wait') ? 'c-tabs__link active' : 'c-tabs__link'
                ]
            ],
            [
                'label'  => 'Обработанные',
                'url'    => ['payment/completed'],
                'linkOptions' => [
                    'class' => strstr(Yii::$app->request->url, 'payment/completed') ? 'c-tabs__link active' : 'c-tabs__link'
                ]
            ],
        ],
    ]
); ?>

    <div class="c-tabs__content tab-content" id="nav-tabContent">
        <div class="c-tabs__pane active">
            <div class="row">
                <div class="col-sm-12">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>