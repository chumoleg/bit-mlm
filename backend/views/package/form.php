<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use mihaildev\ckeditor\CKEditor;
use common\models\currency\Currency;

$this->title = 'Создание/редактирование пакета';

$this->params['breadcrumbs'][] = ['label' => 'Список пакетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->isNewRecord ? 'Новый пакет' : $model->name;

?>

<?php
$form = ActiveForm::begin([
    'enableClientValidation' => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]);
?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'status')->dropDownList($model->getStatusLabels()) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'power')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'profit_currency_id')->dropDownList(Currency::getList()) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'price')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'currency_id')->dropDownList(Currency::getList()) ?>
            </div>
        </div>

        <?= $form->field($model, 'is_hit')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>
        <?= $form->field($model, 'contract_days')->textInput(); ?>
        <?= $form->field($model, 'imageFile')->fileInput(); ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

    <div class="col-md-8">
        <label>Проценты начислений по уровням:</label>
        <div class="row">
            <?php for ($i = 1; $i <= 5; $i++) : ?>
                <div class="col-md-2">
                    <?= $form->field($model, 'percentData[' . $i . ']')
                        ->label(false)
                        ->textInput(['placeholder' => 'Уровень ' . $i]) ?>
                </div>
            <?php endfor; ?>
        </div>

        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'editorOptions' => [
                'language'      => 'ru',
                'preset'        => 'full',
                'height'        => '300px',
                'removeButtons' => 'Flash,Iframe,Image,Form,Checkbox,Radio,TextField,Textarea,Select,Button,
                                Button,ImageButton,HiddenField,Scayt,Replace,Find,About,Format,Styles',
            ],
        ]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
