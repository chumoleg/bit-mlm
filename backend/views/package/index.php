<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;
use common\models\package\Package;

$this->title = 'Управление пакетами';

$this->context->topButtons[] = Html::a(Html::icon('plus') . ' Создать новый пакет', ['create'],
    ['class' => 'c-btn c-btn--info']);

/** @var \common\models\package\Package $data */
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            [
                'attribute' => 'img_src',
                'format'    => 'raw',
                'filter'    => false,
                'value'     => function ($data) {
                    /** @var Package $data */
                    return Html::img($data->getImgSrc(), ['width' => '100px', 'alt' => '...']);
                },
            ],
            [
                'attribute'      => 'name',
                'contentOptions' => ['class' => 'wrappedColumn'],
            ],
            [
                'attribute' => 'power',
                'value'     => function ($data) {
                    /** @var Package $data */
                    return $data->getPowerLabel();
                },
            ],
            [
                'class'     => \common\components\grid\CurrencyColumn::className(),
                'attribute' => 'profit_currency_id',
            ],
            [
                'attribute' => 'price',
                'format'    => 'raw',
                'value'     => function ($data) {
                    return $data->getPriceLabel();
                },
            ],
            'contract_days',
//            [
//                'class'     => \common\components\grid\YesNoColumn::className(),
//                'attribute' => 'is_hit',
//            ],
            [
                'class' => \common\components\grid\StatusColumn::className(),
            ],
//            [
//                'class' => \common\components\grid\DateColumn::className(),
//            ],
            [
                'label'          => 'Проценты начислений',
                'contentOptions' => ['class' => 'wrappedColumn'],
                'value'          => function ($data) {
                    /** @var Package $data */
                    return implode('; ', $data->getPercentArray());
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'options'  => ['width' => '80'],
                'buttons'  => [
                    'update' => function ($url, $model) {

                        $url = \yii\helpers\Url::to(['package/update', 'id' => $model->id]);

                        return Html::a(Html::icon('pencil'), $url, [
                            'title' => 'Редактировать',
                            'class' => 'btn btn-sm btn-default',
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
