<?php

use kartik\form\ActiveForm;

$this->title = 'Авторизация';

?>

<div class="c-login">
    <header class="c-login__head">
        <a class="c-login__brand" href="/">
            <img src="/static/img/Resimico_EN_Black.png" alt="<?= Yii::$app->name; ?>">
        </a>

        <h1 class="c-login__title"><?= $this->title; ?></h1>
    </header>

    <?php $form = ActiveForm::begin([
        'id'          => 'login-form',
        'options'     => [
            'class' => 'c-login__content',
        ],
        'fieldConfig' => [
            'addClass' => 'form-control c-input',
        ],
    ]); ?>

    <div class="c-field u-mb-small">
        <?= $form->field($model, 'email', ['labelOptions' => ['class' => 'c-field__label']])->textInput([
            'placeholder' => 'user@example.com',
        ]); ?>
    </div>

    <div class="c-field u-mb-small">
        <?= $form->field($model, 'password', ['labelOptions' => ['class' => 'c-field__label']])->passwordInput([
            'placeholder' => 'Numbers, Letters...',
        ]); ?>
    </div>

    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Войти в личный кабинет</button>

    <?php ActiveForm::end(); ?>
</div>