<?php

$this->title = 'Аналитика';

$chartHelper = new \common\components\ChartHelper();

$chartView = '@common/views/chart';

?>

<div class="row">
    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render('_user-chart', $chartHelper->getUserRegisterData()); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render($chartView, $chartHelper->getPackageBuyingData()); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render($chartView, $chartHelper->getUserBalanceHistory()); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render($chartView, $chartHelper->getUserProfitData()); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render($chartView, $chartHelper->getMiningPowerData()); ?>
        </div>
    </div>

    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render($chartView, $chartHelper->getMiningProfitData()); ?>
        </div>
    </div>
</div>