<?php

use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
    'options' => [
        'chart'       => [
            'height' => 350,
            'type'   => 'column'
        ],
        'title'       => [
            'text'    => 'Регистрация пользователей',
            'useHTML' => true,
        ],
        'xAxis'       => [
            'categories' => $xAxisCategories,
            'crosshair'  => true
        ],
        'yAxis'       => [
            'title' => ['text' => 'Кол-во'],
//            'stackLabels' => [
//                'enabled' => true,
//            ]
        ],
        'tooltip'     => [
            'headerFormat' => '<b>{point.x}</b><br/>',
        ],
        'plotOptions' => [
            'column' => [
                'stacking' => 'normal',
            ]
        ],
        'series'      => $series,
    ],
]);