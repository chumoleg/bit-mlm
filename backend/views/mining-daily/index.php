<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

$this->title = 'Текущие мощности майнеров';

$this->context->topButtons[] = Html::a(Html::icon('plus') . ' Внести новые данные',
    ['create'], ['class' => 'c-btn c-btn--info']);
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            [
                'class' => \common\components\grid\CurrencyColumn::className(),
            ],
            'power',
            'profit',
            [
                'class'     => \common\components\grid\DateColumn::className(),
                'attribute' => 'date',
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'options'  => ['width' => '100'],
                'buttons'  => [
                    'update' => function ($url, $model) {
                        if ($model->hasFinanceOperations()) {
                            return null;
                        }

                        $url = \yii\helpers\Url::to(['update', 'id' => $model->id]);

                        return Html::a(Html::icon('pencil'), $url, [
                            'title' => 'Редактировать',
                            'class' => 'btn btn-sm btn-default',
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        if ($model->hasFinanceOperations()) {
                            return null;
                        }

                        $url = \yii\helpers\Url::to(['delete', 'id' => $model->id]);

                        return Html::a(Html::icon('remove'), $url, [
                            'title' => 'Удалить',
                            'class' => 'btn btn-sm btn-default',
                            'data-confirm' => 'Вы уверены, что хотите удалить эти данные?'
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
