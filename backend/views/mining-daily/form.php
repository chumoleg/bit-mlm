<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Внесение данных';

$this->params['breadcrumbs'][] = ['label' => 'Текущий список мощностей', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Внесение данных';
?>

<?php
$form = ActiveForm::begin(['enableClientValidation' => false]);
?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'currency_id')->dropDownList(common\models\currency\Currency::getList()) ?>
        <?= $form->field($model, 'power')->textInput() ?>
        <?= $form->field($model, 'profit')->textInput() ?>
        <?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::className()) ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
