<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;

$this->title = 'Уведомления пользователей';

$this->context->topButtons[] = Html::a(Html::icon('plus') . ' Создать новое уведомление', ['create'],
    ['class' => 'c-btn c-btn--info']);
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            [
                'attribute' => 'userEmail',
                'value'     => 'user.email',
            ],
            'message',
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
