<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Создание нового уведомления';
?>

<?php
$form = ActiveForm::begin(['enableClientValidation' => false]);
?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'userEmail')->textInput() ?>
        <?= $form->field($model, 'message')->textInput() ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
