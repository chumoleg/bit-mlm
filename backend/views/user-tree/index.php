<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Дерево пользователей';
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            'userEmail',
            'partnerEmail',
            'level',
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
