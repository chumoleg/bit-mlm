<?php

use yii\grid\GridView;
use common\models\user\User;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

$this->title = 'Управление пользователями';
?>

<div class="c-card u-p-small u-mb-small">
    <?php
    Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            'email',
            'invite_code',
            [
                'attribute' => 'role',
                'filter'    => User::$roleList,
                'value'     => function ($data) {
                    return \yii\helpers\ArrayHelper::getValue(User::$roleList, $data->role);
                },
            ],
            [
                'class' => \common\components\grid\StatusColumn::className(),
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
            [
                'class'     => \common\components\grid\DateColumn::className(),
                'attribute' => 'last_visit',
            ],
            'countPackages',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{update} {login}',
                'options'  => ['width' => '90'],
                'buttons'  => [
                    'update' => function ($url, $model) {

                        $url = \yii\helpers\Url::to(['user/update', 'id' => $model->id]);

                        return Html::a(Html::icon('pencil'), $url, [
                            'title' => 'Редактировать',
                            'class' => 'btn btn-sm btn-default',
                        ]);
                    },
                    'login'  => function ($url, $model) {
                        if ($model->status != User::STATUS_ACTIVE) {
                            return null;
                        }

                        $url = \yii\helpers\Url::to(['user/switch-identity', 'id' => $model->id]);

                        return Html::a(Html::icon('home'), $url, [
                            'title'  => 'Войти в личный кабинет',
                            'target' => '_blank',
                            'class'  => 'btn btn-sm btn-default',
                        ]);
                    },
                ],
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>
