<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\user\User;

$this->title = 'Редактирование пользователя';

$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => $this->context->getIndexUrl()];
$this->params['breadcrumbs'][] = $model->email;

?>

<div class="col-md-4">
    <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>

    <?= $form->field($model, 'email')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList($model->getStatusLabels()) ?>
    <?= $form->field($model, 'role')->dropDownList(User::$roleList) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
