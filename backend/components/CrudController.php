<?php

namespace backend\components;

use Yii;
use yii\web\NotFoundHttpException;

abstract class CrudController extends BaseController
{
    public function actionIndex()
    {
        $className = $this->_getSearchClassName();

        $searchModel  = new $className();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->_rememberIndexUrl();

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    abstract protected function _getSearchClassName();

    public function actionCreate()
    {
        $className = $this->_getFormClassName();

        $model = new $className();
        if ($model->load(Yii::$app->request->post())) {

            if (method_exists($this, '_beforeValidateModel')) {
                $this->_beforeValidateModel($model);
            }

            if ($model->saveForm()) {
                return $this->redirect($this->getIndexUrl());
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    abstract protected function _getFormClassName();

    public function actionUpdate($id)
    {
        $model = $this->_loadModel($id);
        if ($model->load(Yii::$app->request->post())) {

            if (method_exists($this, '_beforeValidateModel')) {
                $this->_beforeValidateModel($model);
            }

            if ($model->saveForm()) {
                return $this->redirect($this->getIndexUrl());
            }
        }

        return $this->render('form', [
            'model' => $model,
        ]);
    }

    /**
     * @param int $id
     *
     * @return static|null
     * @throws NotFoundHttpException
     */
    protected function _loadModel($id)
    {
        $className = $this->_getFormClassName();

        $model = $className::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException('Model not found');
        }

        return $model;
    }
}