<?php

namespace backend\components;

use Yii;
use common\models\user\User;

class BaseController extends \common\components\controllers\BaseController
{
    public $topButtons = [];

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (Yii::$app->user->isGuest || Yii::$app->user->identity->role !== User::ROLE_ADMIN) {
            Yii::$app->user->logout();
            $this->redirect('/');

            return false;
        }

        (new User())->updateVisitData();

        return true;
    }
}