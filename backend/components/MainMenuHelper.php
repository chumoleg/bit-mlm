<?php

namespace backend\components;

use Yii;
use common\models\payment\UserPaymentRequest;

class MainMenuHelper
{
    public static function getPaymentRequestBadge()
    {
        $newRequestCount = UserPaymentRequest::find()
            ->andWhere(['status' => UserPaymentRequest::STATUS_NEW])
            ->count();

        if ($newRequestCount == 0) {
            return '';
        }

        return ' <span class="c-badge c-badge--danger">' . $newRequestCount . '</span>';
    }
}