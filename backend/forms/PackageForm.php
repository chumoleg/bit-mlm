<?php

namespace backend\forms;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Box;
use common\models\package\Package;
use common\models\package\PackagePercent;

class PackageForm extends Package
{
    public $percentData = [];

    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['img_src', 'percentData'], 'safe'],
        ]);
    }

    public function saveForm()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->save()) {
            $this->_savePercentData($this->id);
            $this->_saveImage();

            return true;
        }

        return false;
    }

    /**
     * @param int $packageId
     */
    private function _savePercentData($packageId)
    {
        PackagePercent::deleteAll(['package_id' => $packageId]);
        
        foreach ($this->percentData as $k => $v) {
            $model             = new PackagePercent();
            $model->package_id = $packageId;
            $model->level      = $k;
            $model->percent    = $v;
            $model->save();
        }
    }

    private function _saveImage()
    {
        if (!empty($this->imageFile) && !empty($this->imageFile->baseName)) {

            $path = $this->getImgPath();

            if (!empty($this->img_src)) {
                $oldFilePath = $path . $this->img_src;
                if (file_exists($oldFilePath)) {
                    unlink($oldFilePath);
                }
            }

            $fileName = $this->id . '-' . time() . '.' . $this->imageFile->extension;
            $filePath = $path . $fileName;
            if ($this->imageFile->saveAs($filePath)) {
                $width  = 200;
                $height = 200;
                Image::thumbnail($filePath, $width, $height, ManipulatorInterface::THUMBNAIL_INSET)
                    ->resize(new Box($width, $height))
                    ->save($filePath);

                $this->img_src = $fileName;
                $this->save(false);
            }
        }
    }
}