<?php

namespace backend\forms;

use Yii;
use common\models\user\User;

class UserForm extends User
{
    public function saveForm()
    {
        $model         = self::findOne($this->id);
        $model->email  = $this->email;
        $model->status = $this->status;
        $model->role   = $this->role;

        return $model->save();
    }
}