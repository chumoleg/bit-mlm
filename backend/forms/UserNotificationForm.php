<?php

namespace backend\forms;

use Yii;
use common\models\user\User;
use common\models\userNotification\UserNotification;

class UserNotificationForm extends UserNotification
{
    public $userEmail;

    public function rules()
    {
        return [
            [['userEmail', 'message'], 'required'],
            [
                'userEmail',
                'exist',
                'targetAttribute' => 'email',
                'targetClass'     => User::className(),
                'message'         => 'Пользователь не найден',
            ],
        ];
    }

    /**
     * @return bool
     */
    public function saveForm()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findOne(['email' => $this->userEmail]);

        return parent::createNotification($user->id, $this->message);
    }
}