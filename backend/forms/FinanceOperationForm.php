<?php

namespace backend\forms;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\financeOperation\FinanceOperation;
use common\models\user\User;

class FinanceOperationForm extends FinanceOperation
{
    public $userEmail;

    /**
     * @var User
     */
    private $_user;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['userEmail'], 'required'],
            [['userEmail'], 'validateUserEmail'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userEmail' => 'Email пользователя',
        ]);
    }

    public function beforeValidate()
    {
        if (!empty($this->userEmail)) {
            $this->_user = User::findByEmail($this->userEmail);
            if (!empty($this->_user)) {
                $this->user_id = $this->_user->id;
            }
        }

        return parent::beforeValidate();
    }

    public function validateUserEmail($attribute)
    {
        if (empty($this->_user)) {
            $this->addError($attribute, 'Пользователь не найден или заблокирован');
        }

        return true;
    }

    /**
     * @return bool
     */
    public function saveForm()
    {
        if (!$this->validate()) {
            return false;
        }

        if ($this->type_operation == self::TYPE_OPERATION_WRITTEN) {
            return parent::createWrittenOperation(
                $this->user_id, $this->currency_id, $this->amount, $this->comment);

        } else {
            return parent::createIncomeOperation(
                $this->user_id, $this->currency_id, $this->amount, $this->comment);
        }
    }
}