<?php

namespace backend\forms;

use Yii;
use common\models\currency\Currency;

class CurrencyForm extends Currency
{
    /**
     * @return bool
     */
    public function saveForm()
    {
        if (!$this->validate()) {
            return false;
        }

        return $this->save();
    }
}