<?php

namespace backend\forms;

use Yii;
use common\models\mining\MiningDaily;
use yii\helpers\ArrayHelper;

class MiningDailyForm extends MiningDaily
{
    /**
     * @return bool
     */
    public function saveForm()
    {
        if (!$this->validate()) {
            return false;
        }

        $model = parent::findOne([
            'date'        => $this->date,
            'currency_id' => $this->currency_id,
        ]);

        if (empty($model)) {
            $model              = new parent();
            $model->currency_id = $this->currency_id;
            $model->date        = $this->date;

        } else {
            if ($model->hasFinanceOperations()) {
                $this->addError('date', 'На этот день уже начислены доходы. Сохранение невозможно.');

                return false;
            }
        }

        $model->power  = $this->power;
        $model->profit = $this->profit;

        return $model->save();
    }
}
