<?php

use yii\helpers\Html;

?>

Здравствуйте, <?= Html::encode($user->email); ?>

Вы успешно зарегистрированы в
<?= Html::a(Yii::$app->name, Yii::$app->request->getHostInfo()); ?>

<?php if ($sendConfirmLink) : ?>
    Для подтверждения E-mail адреса перейдите по указанной ссылке:
    <?= $user->getConfirmLink(); ?>
<?php endif; ?>

Данные для входа:
Email: <?= Html::encode($user->email); ?>
Пароль: <?= Html::encode($password); ?>