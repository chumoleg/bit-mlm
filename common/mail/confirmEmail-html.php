<?php

use yii\helpers\Html;

$confirmLink = $user->getConfirmLink();
?>

<div class="confirm-link">
    <p>Здравствуйте!</p>
    <p>Для подтверждения Email перейдите по ссылке:</p>

    <p><?= Html::a(Html::encode($confirmLink), $confirmLink); ?></p>
</div>