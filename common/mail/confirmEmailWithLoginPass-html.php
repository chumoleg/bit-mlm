<?php

use yii\helpers\Html;

$confirmLink = $user->getConfirmLink();
?>

<div class="confirm-link">
    <p>Здравствуйте, <?= Html::encode($user->email); ?></p>

    <p>
        Вы успешно зарегистрированы в
        <?= Html::a(Yii::$app->name, Yii::$app->request->getHostInfo()); ?>
    </p>

    <?php if ($sendConfirmLink) : ?>
        <p>Для подтверждения E-mail адреса перейдите по указанной ссылке:</p>
        <p><?= Html::a(Html::encode($confirmLink), $confirmLink); ?></p>
    <?php endif; ?>

    <p>
        Данные для входа:<br/>
        Email: <?= Html::encode($user->email) ?><br/>
        Пароль: <?= Html::encode($password) ?>
    </p>
</div>