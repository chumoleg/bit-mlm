<?php

namespace common\components;

use Yii;
use yii\helpers\ArrayHelper;

class UrlHelper
{
    /**
     * @return bool
     */
    public static function getFrontendHost()
    {
        $fromParams = ArrayHelper::getValue(Yii::$app->params, 'frontendHost');
        if (!empty($fromParams)) {
            return $fromParams;
        }

        if (isset($_SERVER['HTTP_HOST'])) {
            $hostNames = explode('.', $_SERVER['HTTP_HOST']);

            return 'http://' . $hostNames[count($hostNames) - 2] . '.' . $hostNames[count($hostNames) - 1];
        }

        return $fromParams;
    }
}