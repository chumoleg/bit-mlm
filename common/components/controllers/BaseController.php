<?php

namespace common\components\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class BaseController extends Controller
{
    public $showTitle  = true;
    public $showFooter = true;

    protected function _rememberIndexUrl()
    {
        Url::remember(Yii::$app->request->url, $this->getUniqueId());
    }

    public function getIndexUrl()
    {
        $redirectUrl = Url::previous($this->getUniqueId());
        if (empty($redirectUrl)) {
            $redirectUrl = ['index'];
        }

        return $redirectUrl;
    }
}