<?php

namespace common\components\grid;

use Yii;
use common\widgets\DateRangePicker;

class DateColumn extends DataColumn
{
    public $attribute = 'created_at';
//    public $format = 'datetime';
    public $contentOptions = ['class' => 'date-range-column'];

    public function renderFilterCell()
    {
        $this->filter = DateRangePicker::widget([
            'model'     => $this->grid->filterModel,
            'attribute' => $this->attribute,
        ]);

        return parent::renderFilterCell();
    }
}