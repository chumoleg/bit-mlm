<?php

namespace common\components\grid;

use Yii;
use yii\helpers\ArrayHelper;

class YesNoColumn extends DataColumn
{
    /**
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     *
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return ArrayHelper::getValue($this->_getArray(), $model->{$this->attribute});
    }

    /**
     * @return string
     */
    public function renderFilterCell()
    {
        $this->filter = $this->_getArray();

        return parent::renderFilterCell();
    }

    /**
     * @return array
     */
    private function _getArray()
    {
        return [
            1 => 'Да',
            0 => 'Нет',
        ];
    }
}