<?php

namespace common\components\grid;

use Yii;

class StatusColumn extends DataColumn
{
    public $attribute = 'status';

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     *
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $this->value = [$model, 'getStatusLabel'];

        return parent::renderDataCellContent($model, $key, $index);
    }

    public function renderFilterCell()
    {
        $this->filter = $this->grid->filterModel->getStatusLabels();

        return parent::renderFilterCell();
    }
}