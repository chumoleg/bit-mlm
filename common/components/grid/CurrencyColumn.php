<?php

namespace common\components\grid;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\currency\Currency;

class CurrencyColumn extends DataColumn
{
    public $attribute = 'currency_id';

     /**
     * @param mixed $model
     * @param mixed $key
     * @param int   $index
     *
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        return ArrayHelper::getValue($this->_getArray(), $model->{$this->attribute});
    }

    /**
     * @return string
     */
    public function renderFilterCell()
    {
        $this->filter = $this->_getArray();

        return parent::renderFilterCell();
    }
    
     /**
     * @return array
     */
    private function _getArray()
    {
        return Currency::getList();
    }
}