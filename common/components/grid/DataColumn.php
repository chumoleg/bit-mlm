<?php

namespace common\components\grid;

use Yii;

class DataColumn extends \yii\grid\DataColumn
{
    public function init()
    {
        parent::init();

        if (!empty($this->attribute)) {
            if (empty($this->headerOptions['class'])) {
                $this->headerOptions['class'] = '';
            }

            if (empty($this->filterOptions['class'])) {
                $this->filterOptions['class'] = '';
            }

            if (empty($this->contentOptions['class'])) {
                $this->contentOptions['class'] = '';
            }

            $this->headerOptions['class'] .= ' c-table__cell c-table__cell--head';
            $this->filterOptions['class'] .= ' c-table__cell c-table__cell--head';
            $this->contentOptions['class'] .= ' c-table__cell';
        }
    }
}