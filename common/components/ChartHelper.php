<?php

namespace common\components;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use common\models\user\User;
use common\models\currency\Currency;
use common\models\mining\MiningDaily;
use common\models\package\Package;
use common\models\userPackage\UserPackage;
use common\models\userBalance\UserBalanceHistory;

class ChartHelper
{
    const DEFAULT_DATE_FROM = '2018-03-01';

    /**
     * @return array
     */
    public function getUserRegisterData()
    {
        $data = (new Query())
            ->select([
                'status',
                'date'  => 'DATE(created_at)',
                'count' => 'COUNT(id)',
            ])
            ->from(User::tableName())
            ->groupBy(['date', 'status'])
            ->createCommand()
            ->queryAll();

        $dates = $this->_getDatesFromData($data);

        $data = ArrayHelper::map($data, 'date', 'count', 'status');

        $statusList = (new User)->getStatusLabels();

        $array        = [];
        $arrayDynamic = [];

        $counter = 0;
        foreach ($dates as $date) {

            foreach ($statusList as $statusKey => $statusLabel) {
                $statusData = ArrayHelper::getValue($data, $statusKey, []);

                $count = (int)ArrayHelper::getValue($statusData, $date, 0);

                $array[$statusKey][] = $count;

                $counter += $count;
            }

            $arrayDynamic[] = $counter;
        }

        $series = [];
        foreach ($statusList as $statusKey => $statusLabel) {
            $series[] = [
                'name'    => $statusLabel,
                'data'    => ArrayHelper::getValue($array, $statusKey, []),
                'type'    => 'column',
                'tooltip' => [
                    'useHTML'     => true,
                    'pointFormat' => '{series.name}: {point.y}<br/>Всего пользователей: {point.stackTotal}',
                ],
            ];
        }

        $series[] = [
            'name'    => 'Динамика роста',
            'data'    => $arrayDynamic,
            'type'    => 'spline',
            'visible' => false,
        ];

        return [
            'xAxisCategories' => $dates,
            'series'          => $series,
        ];
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function _getDatesFromData($data)
    {
        $dates = array_unique(ArrayHelper::getColumn($data, 'date'));

        usort($dates, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        $fromDate = reset($dates);

        return $this->_getDatesArray($fromDate);
    }

    /**
     * @param string|null $dateFrom
     *
     * @return array
     */
    private function _getDatesArray($dateFrom = null)
    {
        if (empty($dateFrom)) {
            $dateFrom = self::DEFAULT_DATE_FROM;

        } else {
            $dateFrom = date('Y-m-d', strtotime($dateFrom . ' -1 day'));
        }

        $date = $dateFrom;

        $array = [];
        while (true) {
            $dateTo = date('Y-m-d');
            if ($date > $dateTo) {
                break;
            }

            $array[] = $date;

            $date = date('Y-m-d', strtotime($date . ' + 1 day'));
        }

        return $array;
    }

    /**
     * @param int|null $userId
     *
     * @return array
     */
    public function getUserProfitData($userId = null)
    {
        $query = (new Query())
            ->select([
                'date'      => 'date',
                'sumAmount' => 'SUM(profit)',
                'currency'  => 'currency_id',
            ])
            ->from(UserBalanceHistory::tableName())
            ->groupBy(['date', 'currency']);

        if (!empty($userId)) {
            $query->andWhere(['user_id' => $userId]);
        }

        $data = $query->createCommand()->queryAll();

        $title = 'Динамика дохода пользователей';
        if (!empty($userId)) {
            $title = 'Динамика дохода';
        }

        return $this->_getFinanceArray($title, $data);
    }

    /**
     * @param string $title
     * @param array  $data
     *
     * @return array
     */
    private function _getFinanceArray($title, $data)
    {
        $dates = $this->_getDatesFromData($data);
        $data  = ArrayHelper::map($data, 'date', 'sumAmount', 'currency');

        $currencyList = Currency::getList();

        $array = [];
        foreach ($dates as $date) {
            foreach ($currencyList as $currencyId => $currencyAlias) {
                $sumAmountData = ArrayHelper::getValue($data, $currencyId, []);

                $amount = (float)ArrayHelper::getValue($sumAmountData, $date, 0);

                $array[$currencyId][] = [
                    'y'     => $amount,
                    'label' => $amount . ' ' . $currencyAlias,
                ];
            }
        }

        $series = [];
        foreach ($array as $currencyId => $seriesData) {
            $series[] = [
                'name'    => ArrayHelper::getValue($currencyList, $currencyId, '-'),
                'data'    => $seriesData,
                'type'    => 'spline',
                'tooltip' => [
                    'useHTML'     => true,
                    'pointFormat' => '<b>{series.name}</b><br />Сумма: <b>{point.label}</b>',
                ],
            ];
        }

        return $this->_prepareArray($title, $dates, $series, 'Сумма');
    }

    /**
     * @param int    $height
     * @param string $title
     * @param string $yAxisTitle
     * @param array  $xAxisCategories
     * @param array  $series
     *
     * @return array
     */
    private function _prepareArray($title, $xAxisCategories = [], $series = [], $yAxisTitle = 'Кол-во', $height = 350)
    {
        return [
            'chartHeight'     => $height,
            'title'           => $title,
            'yAxisTitle'      => $yAxisTitle,
            'xAxisCategories' => $xAxisCategories,
            'series'          => $series,
        ];
    }

    /**
     * @param int|null $userId
     *
     * @return array
     */
    public function getUserBalanceHistory($userId = null)
    {
        $query = (new Query())
            ->select([
                'date'      => 'date',
                'sumAmount' => 'SUM(balance)',
                'currency'  => 'currency_id',
            ])
            ->from(UserBalanceHistory::tableName())
            ->groupBy(['date', 'currency']);

        if (!empty($userId)) {
            $query->andWhere(['user_id' => $userId]);
        }

        $data = $query->createCommand()->queryAll();

        $title = 'Динамика балансов пользователей';
        if (!empty($userId)) {
            $title = 'Динамика баланса';
        }

        return $this->_getFinanceArray($title, $data);
    }

    /**
     * @return array
     */
    public function getPackageBuyingData()
    {
        $data = (new Query())
            ->select([
                'date'      => 'DATE(created_at)',
                'count'     => 'COUNT(id)',
                'sumAmount' => 'SUM(price)',
                'package'   => 'package_id',
            ])
            ->from(UserPackage::tableName())
            ->groupBy(['date', 'package'])
            ->createCommand()
            ->queryAll();

        $dates = $this->_getDatesFromData($data);

        $dataCount     = ArrayHelper::map($data, 'date', 'count', 'package');
        $dataSumAmount = ArrayHelper::map($data, 'date', 'sumAmount', 'package');

        /** @var Package[] $packages */
        $packages = Package::find()->all();

        $array = [];
        foreach ($dates as $date) {
            foreach ($packages as $package) {
                $packageId = $package->id;

                $countData = ArrayHelper::getValue($dataCount, $packageId, []);
                $sumData   = ArrayHelper::getValue($dataSumAmount, $packageId, []);

                $sum   = (float)ArrayHelper::getValue($sumData, $date, 0);
                $count = (int)ArrayHelper::getValue($countData, $date, 0);

                $array[$packageId][] = [
                    'y'        => $count,
                    'package'  => $package->name,
                    'count'    => $count,
                    'sumLabel' => $sum . ' ' . $package->profitCurrency->alias,
                ];
            }
        }

        $packageList = ArrayHelper::map($packages, 'id', 'name');

        $series = [];
        foreach ($array as $packageId => $seriesData) {
            $series[] = [
                'name'    => ArrayHelper::getValue($packageList, $packageId, '-'),
                'data'    => $seriesData,
                'type'    => 'line',
                'tooltip' => [
                    'useHTML'     => true,
                    'pointFormat' => '<b>{point.package}</b><br />Куплено: <b>{point.count}</b><br />На сумму: <b>{point.sumLabel}</b>',
                ],
            ];
        }

        return $this->_prepareArray('Покупки пакетов', $dates, $series, 'Кол-во');
    }

    /**
     * @return array
     */
    public function getMiningPowerData()
    {
        $data = (new Query())
            ->select([
                'date',
                'power',
                'currency_id',
            ])
            ->from(MiningDaily::tableName())
            ->createCommand()
            ->queryAll();

        $dates = $this->_getDatesFromData($data);
        $data  = ArrayHelper::map($data, 'date', 'power', 'currency_id');

        /** @var Currency[] $currencies */
        $currencies = Currency::find()->all();

        $array = [];
        foreach ($dates as $date) {
            foreach ($currencies as $currency) {
                $currencyId = $currency->id;

                $powerData = ArrayHelper::getValue($data, $currencyId, []);

                $power = (float)ArrayHelper::getValue($powerData, $date, 0);

                $array[$currencyId][] = [
                    'y'     => $power,
                    'label' => $power . ' ' . $currency->getPowerTypeLabel(),
                ];
            }
        }

        $currencyList = ArrayHelper::map($currencies, 'id', 'alias');

        $series = [];
        foreach ($array as $currencyId => $seriesData) {
            $series[] = [
                'name'    => ArrayHelper::getValue($currencyList, $currencyId, '-'),
                'data'    => $seriesData,
                'type'    => 'spline',
                'tooltip' => [
                    'useHTML'     => true,
                    'pointFormat' => '<b>{series.name}</b><br />Мощность: <b>{point.label}</b>',
                ],
            ];
        }

        return $this->_prepareArray('Мощности майнеров', $dates, $series, 'Мощность');
    }

    /**
     * @return array
     */
    public function getMiningProfitData()
    {
        $data = (new Query())
            ->select([
                'date',
                'profit',
                'currency_id',
            ])
            ->from(MiningDaily::tableName())
            ->createCommand()
            ->queryAll();

        $dates = $this->_getDatesFromData($data);
        $data  = ArrayHelper::map($data, 'date', 'profit', 'currency_id');

        $currencyList = Currency::getList();

        $array = [];
        foreach ($dates as $date) {
            foreach ($currencyList as $currencyId => $currencyAlias) {

                $profitData = ArrayHelper::getValue($data, $currencyId, []);

                $profit = (float)ArrayHelper::getValue($profitData, $date, 0);

                $array[$currencyId][] = [
                    'y'     => $profit,
                    'label' => $profit . ' ' . $currencyAlias,
                ];
            }
        }

        $series = [];
        foreach ($array as $currencyId => $seriesData) {
            $series[] = [
                'name'    => ArrayHelper::getValue($currencyList, $currencyId, '-'),
                'data'    => $seriesData,
                'type'    => 'column',
                'tooltip' => [
                    'useHTML'     => true,
                    'pointFormat' => '<b>{series.name}</b><br />Заработано: <b>{point.label}</b>',
                ],
            ];
        }

        return $this->_prepareArray('Прибыль майнеров', $dates, $series, 'Сумма');
    }
}