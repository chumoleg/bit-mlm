<?php

namespace common\components\behaviors;

use Yii;
use yii\db\BaseActiveRecord;
use yii\db\Expression;

class TimestampBehavior extends \yii\base\Behavior
{
    public $createAttribute = 'created_at';
    public $updateAttribute = 'updated_at';

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => [$this, 'handlerBeforeInsert'],
            BaseActiveRecord::EVENT_BEFORE_UPDATE => [$this, 'handlerBeforeUpdate'],
        ];
    }

    public function handlerBeforeInsert($event)
    {
        $this->_updateAttribute($event, $this->createAttribute);
    }

    /**
     * @param $event
     * @param $attribute
     */
    private function _updateAttribute($event, $attribute)
    {
        $sender = $event->sender;

        if ($sender->hasAttribute($attribute) && empty($sender->getAttribute($attribute))) {
            $sender->setAttribute($attribute, new Expression('NOW()'));
        }
    }

    public function handlerBeforeUpdate($event)
    {
        $this->_updateAttribute($event, $this->updateAttribute);
    }
}
