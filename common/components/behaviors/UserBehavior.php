<?php

namespace common\components\behaviors;

use Yii;
use yii\db\BaseActiveRecord;

class UserBehavior extends \yii\base\Behavior
{
    public $attributeName = 'user_id';

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => [$this, 'handlerBeforeInsert'],
        ];
    }

    public function handlerBeforeInsert($event)
    {
        if (!empty(Yii::$app->user) && !empty(Yii::$app->session) && !Yii::$app->user->isGuest) {
            $sender = $event->sender;

            if ($sender->hasAttribute($this->attributeName) && empty($sender->getAttribute($this->attributeName))) {
                $sender->setAttribute($this->attributeName, Yii::$app->user->id);
            }
        }
    }
}
