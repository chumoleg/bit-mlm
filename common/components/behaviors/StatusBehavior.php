<?php

namespace common\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;

class StatusBehavior extends Behavior
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_ACTIVE = 0;

    public $attributeName = 'status';

    public $defaultStatus = self::STATUS_ACTIVE;

    public $statuses = [
        self::STATUS_ACTIVE,
        self::STATUS_NOT_ACTIVE,
    ];

    public $statusLabels = [];

    /**
     * @return array
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => [$this, 'handlerBeforeInsert'],
        ];
    }

    /**
     * @param $event
     */
    public function handlerBeforeInsert($event)
    {
        $sender = $event->sender;

        if ($sender->hasAttribute($this->attributeName) && is_null($sender->getAttribute($this->attributeName))) {
            $sender->setAttribute($this->attributeName, $this->defaultStatus);
        }
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        return ArrayHelper::getValue($this->getStatusLabels(), $this->owner->{$this->attributeName});
    }

    /**
     * @return array
     */
    public function getStatusLabels()
    {
        if (empty($this->statusLabels)) {
            return $this->_getDefaultStatusLabels();
        }

        return $this->statusLabels;
    }

    /**
     * @return array
     */
    private function _getDefaultStatusLabels()
    {
        return [
            self::STATUS_ACTIVE     => 'Активный',
            self::STATUS_NOT_ACTIVE => 'Не активный',
        ];
    }

    public function setNotActive()
    {
        $this->owner->{$this->attributeName} = self::STATUS_NOT_ACTIVE;

        return $this->owner;
    }

    public function setActive()
    {
        $this->owner->{$this->attributeName} = self::STATUS_ACTIVE;

        return $this->owner;
    }

    public function isActive()
    {
        return $this->owner->{$this->attributeName} == self::STATUS_ACTIVE;
    }

    public function isNotActive()
    {
        return $this->owner->{$this->attributeName} == self::STATUS_NOT_ACTIVE;
    }
}
