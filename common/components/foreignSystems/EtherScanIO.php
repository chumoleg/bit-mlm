<?php

namespace common\components\foreignSystems;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\currency\Currency;

class EtherScanIO
{
    const BASE_URL = 'http://api.etherscan.io/api';
    const API_KEY = 'RRX8WVANWUMP92VUUCBHSPUKHMVQSF7DPK';

    /**
     * @param Currency $currency
     *
     * @return array
     */
    public static function getListTransactions(Currency $currency)
    {
        if (empty($currency->receiving_purse)) {
            return [];
        }
        
        $params = [
            'module'  => 'account',
            'action'  => 'txlist',
            'address' => $currency->receiving_purse,
            'sort'    => 'desc',
            'page'    => 1,
            'offset'  => 100,
            'apikey'  => self::API_KEY,
        ];

        $url = self::BASE_URL . '?' . http_build_query($params);
        $answer = Yii::$app->curl->get($url);

        $data = Json::decode($answer);

        return ArrayHelper::getValue($data, 'result');
    }
}