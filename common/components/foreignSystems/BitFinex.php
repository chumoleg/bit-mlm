<?php

namespace common\components\foreignSystems;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class BitFinex
{
    const BASE_URL = 'https://api.bitfinex.com/v1/pubticker';

    /**
     * @param float  $amount
     * @param string $currencyAlias
     *
     * @return float
     */
    public static function getEqualUsd($amount, $currencyAlias)
    {
        return $amount * self::_getExchangeRate($currencyAlias);
    }

    /**
     * @param string $currencyAlias
     *
     * @return float
     */
    private static function _getExchangeRate($currencyAlias)
    {
        $cacheKey = 'exchangeRate_' . $currencyAlias . 'USD';
        $price = Yii::$app->cache->get($cacheKey);
        if ($price !== false) {
            return $price;
        }

        $url = self::BASE_URL . '/' . $currencyAlias . 'USD';

        $data = Yii::$app->curl->get($url);
        if (empty($data)) {
            return 0;
        }

        $array = Json::decode($data);

        $price = ArrayHelper::getValue($array, 'last_price', 0);
        if ($price <= 0) {
            return 0;
        }

        Yii::$app->cache->set($cacheKey, $price, 10 * 60);

        return $price;
    }
}