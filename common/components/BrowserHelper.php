<?php

namespace common\components;

class BrowserHelper
{
    const BROWSER_FIREFOX = 'Firefox';
    const BROWSER_CHROME = 'Chrome';
    const BROWSER_CHROMIUM = 'Chromium';
    const BROWSER_OPERA = 'Opera';
    const BROWSER_YANDEX = 'Yandex';
    const BROWSER_SAFARI = 'Safari';

    /**
     * @return bool
     */
    public static function isChromeBrow()
    {
        return self::getBrowser() == self::BROWSER_CHROME;
    }

    /**
     * @return null|string
     */
    public static function getBrowser()
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        $browser = null;
        if (preg_match('/Firefox/i', $userAgent)) {
            $browser = self::BROWSER_FIREFOX;

        } elseif (preg_match('/YaBrowser/i', $userAgent) || preg_match('/Yowser/i', $userAgent)) {
            $browser = self::BROWSER_YANDEX;

        } elseif (preg_match('/Opera/i', $userAgent) || preg_match('/OPR/i', $userAgent)) {
            $browser = self::BROWSER_OPERA;

        } elseif (preg_match('/Chromium/i', $userAgent)) {
            $browser = self::BROWSER_CHROMIUM;

        } elseif (preg_match('/Chrome/i', $userAgent)) {
            $browser = self::BROWSER_CHROME;

        } elseif (preg_match('/Safari/i', $userAgent)) {
            $browser = self::BROWSER_SAFARI;
        }

        return $browser;
    }

    /**
     * @return array
     */
    public static function getBrowserList()
    {
        $array = [
            self::BROWSER_CHROME,
            self::BROWSER_FIREFOX,
            self::BROWSER_OPERA,
            self::BROWSER_YANDEX,
            self::BROWSER_SAFARI,
            self::BROWSER_CHROMIUM,
        ];

        return array_combine(array_map('strtolower', $array), $array);
    }
}