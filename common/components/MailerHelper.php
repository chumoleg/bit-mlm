<?php

namespace common\components;

use Yii;
use common\models\user\User;

class MailerHelper
{
    /**
     * @param User   $user
     * @param string $passwordText
     * @param bool   $sendConfirmLink
     *
     * @return bool
     */
    public static function sendConfirmationMailWithLoginPass($user, $passwordText, $sendConfirmLink = true)
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'confirmEmailWithLoginPass-html', 'text' => 'confirmEmailWithLoginPass-text'],
                [
                    'user'            => $user,
                    'password'        => $passwordText,
                    'sendConfirmLink' => $sendConfirmLink,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject('Подтверждение Email ' . Yii::$app->name)
            ->send();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public static function sendConfirmationMail($user)
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'confirmEmail-html', 'text' => 'confirmEmail-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject('Подтверждение Email ' . Yii::$app->name)
            ->send();
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public static function sendPasswordResetMail($user)
    {
        return Yii::$app->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($user->email)
            ->setSubject('Сброс пароля ' . Yii::$app->name)
            ->send();
    }
}