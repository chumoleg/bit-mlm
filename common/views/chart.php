<?php

use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
    'options' => [
        'chart'  => [
            'height' => $chartHeight,
        ],
        'title'  => [
            'text'    => $title,
            'useHTML' => true,
        ],
        'xAxis'  => [
            'categories' => $xAxisCategories,
            'crosshair' => true
        ],
        'yAxis'  => [
            'title' => ['text' => $yAxisTitle],
        ],
        'series' => $series,
    ],
]);