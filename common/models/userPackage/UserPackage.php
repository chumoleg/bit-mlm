<?php

namespace common\models\userPackage;

use Yii;
use common\models\user\User;
use common\models\package\Package;
use common\models\currency\Currency;

/**
 * This is the model class for table "user_package".
 *
 * @property int      $id
 * @property int      $user_id
 * @property int      $package_id
 * @property float    $price
 * @property int      $currency_id
 * @property int      $status
 * @property string   $created_at
 * @property string   $contract_last_date
 *
 * @property Package  $package
 * @property Currency $currency
 * @property User     $user
 */
class UserPackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_package';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            \common\components\behaviors\StatusBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'package_id', 'price', 'currency_id', 'contract_last_date'], 'required'],
            [['user_id', 'package_id', 'status'], 'integer'],
            [['price'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'user_id'            => 'Пользователь',
            'package_id'         => 'Пакет',
            'price'              => 'Стоимость покупки',
            'currency_id'        => 'Валюта',
            'status'             => 'Статус',
            'created_at'         => 'Дата покупки',
            'contract_last_date' => 'Дата завершения контракта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param int     $userId
     * @param Package $package
     *
     * @return bool
     */
    public static function createRecord($userId, Package $package)
    {
        $contractDate = date('Y-m-d', strtotime('+2 days'));
        if ($package->contract_days > 0) {
            $contractDate = date('Y-m-d', strtotime('+' . ($package->contract_days + 1) . ' days'));
        }

        $model                     = new self();
        $model->user_id            = $userId;
        $model->package_id         = $package->id;
        $model->price              = $package->price;
        $model->currency_id        = $package->currency_id;
        $model->contract_last_date = $contractDate;

        return $model->save();
    }
}
