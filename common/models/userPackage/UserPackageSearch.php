<?php

namespace common\models\userPackage;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class UserPackageSearch extends UserPackage
{
    public $userEmail;
    public $packageName;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'userEmail', 'packageName'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userEmail'   => 'Пользователь',
            'packageName' => 'Пакет',
        ]);
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->joinWith(['user', 'package'])
            ->andFilterWhere([
                't.id'          => $this->id,
                't.user_id'     => $this->user_id,
                't.package_id'  => $this->package_id,
                't.price'       => $this->price,
                't.currency_id' => $this->currency_id,
                't.status'      => $this->status,
            ])
            ->andFilterWhere(['LIKE', 'user.email', $this->userEmail])
            ->andFilterWhere(['LIKE', 'package.name', $this->packageName]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
