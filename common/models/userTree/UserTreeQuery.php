<?php

namespace common\models\userTree;

use Yii;

/**
 * This is the ActiveQuery class for [[UserTree]].
 *
 * @see UserTree
 */
class UserTreeQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            \creocoder\nestedsets\NestedSetsQueryBehavior::className(),
        ];
    }
}