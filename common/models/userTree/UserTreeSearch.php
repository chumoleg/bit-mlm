<?php

namespace common\models\userTree;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class UserTreeSearch extends UserTree
{
    public $userEmail;
    public $partnerEmail;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'userEmail', 'partnerEmail'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userEmail'    => 'Пользователь',
            'partnerEmail' => 'По чьему инвайт-коду',
        ]);
    }

    private function _getInnerUserTreeQuery()
    {
        return $this->find()
            ->alias('it')
            ->select(['iu.email'])
            ->joinWith(['user iu'])
            ->andWhere('it.lft < t.lft')
            ->andWhere('it.rgt > t.rgt')
            ->andWhere('it.level >= (t.level - 1)')
            ->andWhere('it.level > 0')
            ->addOrderBy(['it.lft' => SORT_ASC]);
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $innerUserTreeQuery = $this->_getInnerUserTreeQuery();

        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->joinWith(['user'])
            ->select([
                't.*',
                'userEmail'    => 'user.email',
                'partnerEmail' => $innerUserTreeQuery,
            ])
            ->andWhere('t.level > 0')
            ->andFilterWhere([
                't.id'      => $this->id,
                't.user_id' => $this->user_id,
                't.level'   => $this->level,
            ])
            ->andFilterWhere(['LIKE', 'user.email', $this->userEmail]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        $query->andFilterHaving(['LIKE', 'partnerEmail', $this->partnerEmail]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
