<?php

namespace common\models\userTree;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "user_tree".
 *
 * @property int    $id
 * @property int    $user_id
 * @property int    $lft
 * @property int    $rgt
 * @property int    $level
 * @property string $created_at
 *
 * @property User   $user
 */
class UserTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_tree';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class'          => \creocoder\nestedsets\NestedSetsBehavior::className(),
                'depthAttribute' => 'level',
            ],
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @return UserTreeQuery|\yii\db\ActiveQuery
     */
    public static function find()
    {
        return new UserTreeQuery(get_called_class());
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['lft', 'rgt', 'level', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'level'      => 'Уровень',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param User $user
     * @param User $parentUser
     *
     * @return bool
     */
    public static function createUserTree($user, $parentUser)
    {
        if (empty($parentUser)) {
            return true;
        }

        $parentTree = self::findOne(['user_id' => $parentUser->id]);

        // Привязываем пользователя к своему родителю
        $newUserTree = new self(['user_id' => $user->id]);
        if ($newUserTree->appendTo($parentTree)) {
            return true;
        }

        return false;
    }
}
