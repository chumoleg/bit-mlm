<?php

namespace common\models\currency;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\financeOperation\FinanceOperation;
use common\models\userBalance\UserBalance;

/**
 * This is the model class for table "currency".
 *
 * @property int                $id
 * @property string             $name
 * @property string             $alias
 * @property int                $power_type
 * @property string             $receiving_purse
 * @property string             $created_at
 *
 * @property FinanceOperation[] $financeOperations
 * @property UserBalance[]      $userBalances
 */
class Currency extends \yii\db\ActiveRecord
{
    const POWER_TYPE_TH = 1;
    const POWER_TYPE_MH = 2;

    public static $powerTypeList
        = [
            self::POWER_TYPE_TH => 'Th',
            self::POWER_TYPE_MH => 'Mh',
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'alias', 'power_type', 'receiving_purse'], 'required'],
            [['power_type'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'alias'], 'string', 'max' => 50],
            [['receiving_purse'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'name'            => 'Название',
            'alias'           => 'Алиас',
            'power_type'      => 'Выражение мощности',
            'receiving_purse' => 'Кошелек для получения платежей',
            'created_at'      => 'Дата создания',
        ];
    }

    /**
     * @return array
     */
    public static function getList()
    {
        return ArrayHelper::map(self::find()->asArray()->all(), 'id', 'alias');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOperations()
    {
        return $this->hasMany(FinanceOperation::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBalances()
    {
        return $this->hasMany(UserBalance::className(), ['currency_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getPowerTypeLabel()
    {
        return ArrayHelper::getValue(self::$powerTypeList, $this->power_type);
    }

    /**
     * @return null|static
     */
    public static function getEth()
    {
        return self::findOne(['alias' => 'ETH']);
    }
}
