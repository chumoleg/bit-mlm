<?php

namespace common\models\currency;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;
use common\models\userBalance\UserBalance;

class CurrencySearch extends Currency
{
    public $sumAmount;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'sumAmount'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'sumAmount' => 'Сумма балансов',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->select([
                't.*',
                'sumAmount' => '(SELECT SUM(balance) FROM ' . UserBalance::tableName() . ' WHERE currency_id = t.id)',
            ])
            ->andFilterWhere([
                't.id'    => $this->id,
                't.name'  => $this->name,
                't.alias' => $this->alias,
            ]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
