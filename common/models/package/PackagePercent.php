<?php

namespace common\models\package;

use Yii;

/**
 * This is the model class for table "package_percent".
 *
 * @property int     $id
 * @property int     $package_id
 * @property int     $level
 * @property string  $percent
 * @property string  $created_at
 *
 * @property Package $package
 */
class PackagePercent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_percent';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'level', 'percent'], 'required'],
            [['package_id', 'level'], 'integer'],
            [['percent'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'package_id' => 'Package ID',
            'level'      => 'Level',
            'percent'    => 'Percent',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }
}
