<?php

namespace common\models\package;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class PackageSearch extends Package
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id'], 'safe'],
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->andFilterWhere([
                't.id'     => $this->id,
                't.status' => $this->status,
            ])
            ->andFilterWhere(['LIKE', 't.name', $this->name]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

}
