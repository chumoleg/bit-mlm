<?php

namespace common\models\package;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\userPackage\UserPackage;
use common\models\currency\Currency;

/**
 * This is the model class for table "package".
 *
 * @property int              $id
 * @property string           $name
 * @property string           $description
 * @property int              $power
 * @property int              $profit_currency_id
 * @property string           $price
 * @property int              $currency_id
 * @property string           $img_src
 * @property int              $is_hit
 * @property int              $contract_days
 * @property int              $status
 * @property string           $created_at
 * @property string           $updated_at
 *
 * @property PackagePercent[] $packagePercents
 * @property UserPackage[]    $userPackages
 * @property Currency         $currency
 * @property Currency         $profitCurrency
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            \common\components\behaviors\StatusBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                    'description',
                    'power',
                    'price',
                    'currency_id',
                    'profit_currency_id',
                    'is_hit',
                    'contract_days',
                ],
                'required',
            ],
            [['description'], 'string'],
            [['currency_id', 'profit_currency_id', 'is_hit', 'status', 'contract_days'], 'integer'],
            [['price', 'power'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 300],
            [['img_src'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'name'               => 'Название',
            'description'        => 'Описание',
            'power'              => 'Мощность',
            'profit_currency_id' => 'Валюта дохода',
            'price'              => 'Цена',
            'currency_id'        => 'Валюта покупки',
            'img_src'            => 'Изображение',
            'is_hit'             => 'Хит',
            'contract_days'      => 'Кол-во дней контракта',
            'status'             => 'Статус',
            'created_at'         => 'Дата создания',
            'updated_at'         => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackagePercents()
    {
        return $this->hasMany(PackagePercent::className(), ['package_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPackages()
    {
        return $this->hasMany(UserPackage::className(), ['package_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfitCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'profit_currency_id']);
    }

    /**
     * @return string
     */
    public function getImgPath()
    {
        return Yii::$app->getBasePath() . '/web/static/img/package/';
    }

    /**
     * @param bool $useAbsoluteUrl
     *
     * @return string
     */
    public function getImgSrc($useAbsoluteUrl = false)
    {
        $imgSrc = '/static/img/project-card1.jpg';
        if (!empty($this->img_src)) {
            $imgSrc = '/static/img/package/' . $this->img_src;
        }

        return ($useAbsoluteUrl ? Yii::$app->request->getHostInfo() : '') . $imgSrc;
    }

    /**
     * @return array
     */
    public function getPercentArray()
    {
        $array = ArrayHelper::map($this->packagePercents, 'level', 'percent');
        ksort($array);

        return $array;
    }

    /**
     * @return string
     */
    public function getPowerLabel()
    {
        return $this->power . ' ' . $this->profitCurrency->getPowerTypeLabel();
    }

    /**
     * @return string
     */
    public function getPriceLabel()
    {
        $currencyAlias = $this->currency->alias;

        $equalUsd = \common\components\foreignSystems\BitFinex::getEqualUsd($this->price, $currencyAlias);

        return rtrim($this->price, '0') . ' ' . $currencyAlias . '<br />~ '
               . round($equalUsd, 2) . 'USD';
    }
}
