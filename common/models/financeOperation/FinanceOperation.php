<?php

namespace common\models\financeOperation;

use common\models\userNotification\UserNotification;
use Yii;
use yii\helpers\ArrayHelper;
use common\models\user\User;
use common\models\currency\Currency;
use common\models\userBalance\UserBalance;
use common\models\mining\MiningDaily;

/**
 * This is the model class for table "finance_operation".
 *
 * @property int              $id
 * @property int              $user_id
 * @property int              $parent_operation_id
 * @property int              $mining_daily_id
 * @property int              $currency_id
 * @property int              $type_operation
 * @property int              $status
 * @property float            $amount
 * @property string           $comment
 * @property string           $created_at
 *
 * @property User             $user
 * @property FinanceOperation $parentOperation
 * @property MiningDaily      $miningDaily
 * @property Currency         $currency
 */
class FinanceOperation extends \yii\db\ActiveRecord
{
    const TYPE_OPERATION_INCOME = 1;
    const TYPE_OPERATION_WRITTEN = 2;

    public static $typeOperationList
        = [
            self::TYPE_OPERATION_INCOME  => 'Поступление',
            self::TYPE_OPERATION_WRITTEN => 'Списание',
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance_operation';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            \common\components\behaviors\StatusBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'currency_id', 'type_operation', 'amount', 'comment'], 'required'],
            [
                ['user_id', 'parent_operation_id', 'currency_id', 'type_operation', 'status', 'mining_daily_id'],
                'integer',
            ],
            [['amount'], 'number'],
            [['comment'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => 'ID',
            'user_id'             => 'Пользователь',
            'parent_operation_id' => 'Начислено от операции',
            'mining_daily_id'     => 'Начисление дохода',
            'currency_id'         => 'Валюта',
            'type_operation'      => 'Тип операции',
            'status'              => 'Статус',
            'amount'              => 'Сумма',
            'comment'             => 'Комментарий',
            'created_at'          => 'Дата операции',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        (new UserBalance)->recalculate($this->user_id, $this->currency_id);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentOperation()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_operation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMiningDaily()
    {
        return $this->hasOne(MiningDaily::className(), ['id' => 'mining_daily_id']);
    }

    /**
     * @return string
     */
    public function getTypeOperationLabel()
    {
        return ArrayHelper::getValue(self::$typeOperationList, $this->type_operation);
    }

    /**
     * @param int      $userId
     * @param int      $currencyId
     * @param float    $amount
     * @param string   $comment
     * @param int|null $parentOperationId
     * @param int|null $miningDailyId
     *
     * @return int|null
     */
    public static function createWrittenOperation(
        $userId,
        $currencyId,
        $amount,
        $comment,
        $parentOperationId = null,
        $miningDailyId = null
    )
    {
        return self::_createOperation(
            $userId,
            self::TYPE_OPERATION_WRITTEN,
            $currencyId,
            $amount,
            $comment,
            $parentOperationId,
            $miningDailyId
        );
    }

    /**
     * @param int      $userId
     * @param int      $currencyId
     * @param float    $amount
     * @param string   $comment
     * @param int|null $parentOperationId
     * @param int|null $miningDailyId
     *
     * @return int|null
     */
    public static function createIncomeOperation(
        $userId,
        $currencyId,
        $amount,
        $comment,
        $parentOperationId = null,
        $miningDailyId = null
    )
    {
        return self::_createOperation(
            $userId,
            self::TYPE_OPERATION_INCOME,
            $currencyId,
            $amount,
            $comment,
            $parentOperationId,
            $miningDailyId
        );
    }

    /**
     * @param int      $userId
     * @param int      $typeOperation
     * @param int      $currencyId
     * @param float    $amount
     * @param string   $comment
     * @param int|null $parentOperationId
     * @param int|null $miningDailyId
     *
     * @return int|null
     */
    private static function _createOperation(
        $userId,
        $typeOperation,
        $currencyId,
        $amount,
        $comment,
        $parentOperationId = null,
        $miningDailyId = null
    )
    {
        $model = new self();
        $model->user_id = $userId;
        $model->type_operation = $typeOperation;
        $model->currency_id = $currencyId;
        $model->amount = $amount;
        $model->comment = $comment;
        $model->parent_operation_id = $parentOperationId;
        $model->mining_daily_id = $miningDailyId;

        if ($model->save()) {
            return $model->id;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getAmountLabel()
    {
        $amount = rtrim($this->amount, '0') . ' ' . $this->currency->alias;

        return $this->type_operation == self::TYPE_OPERATION_INCOME ? '+' . $amount : '-' . $amount;
    }

    /**
     * @return bool
     */
    public function disableOperation()
    {
        if ($this->setNotActive()->save(false)) {
            UserNotification::createNotification($this->user_id, 'Операция ' . $this->id . ' отклонена администратором!');
        }

        return true;
    }
}