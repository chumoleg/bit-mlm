<?php

namespace common\models\financeOperation;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class FinanceOperationSearch extends FinanceOperation
{
    public $userEmail;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'userEmail'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userEmail' => 'Пользователь',
        ]);
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->joinWith(['user'])
            ->andFilterWhere([
                't.id'                  => $this->id,
                't.type_operation'      => $this->type_operation,
                't.status'              => $this->status,
                't.amount'              => $this->amount,
                't.parent_operation_id' => $this->parent_operation_id,
                't.currency_id'         => $this->currency_id,
                't.user_id'             => $this->user_id,
            ])
            ->andFilterWhere(['LIKE', 'user.email', $this->userEmail])
            ->andFilterWhere(['LIKE', 't.comment', $this->comment]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        if (!is_null($this->mining_daily_id) && $this->mining_daily_id != '') {
            if ($this->mining_daily_id == 1) {
                $query->andWhere('t.mining_daily_id IS NOT NULL');
            } else {
                $query->andWhere('t.mining_daily_id IS NULL');
            }
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
