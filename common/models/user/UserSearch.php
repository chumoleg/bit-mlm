<?php

namespace common\models\user;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;
use common\models\userPackage\UserPackage;

class UserSearch extends User
{
    public $countPackages;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'countPackages'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'countPackages' => 'Куплено пакетов',
        ]);
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->select([
                't.*',
                'countPackages' => '(SELECT COUNT(id) FROM ' . UserPackage::tableName()
                                   . ' WHERE user_id = t.id AND status = 1)',
            ])
            ->andFilterWhere([
                't.id'     => $this->id,
                't.status' => $this->status,
                't.role'   => $this->role,
            ])
            ->andFilterWhere(['LIKE', 't.email', $this->email]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        $query->andFilterHaving([
            'countPackages' => $this->countPackages,
        ]);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
