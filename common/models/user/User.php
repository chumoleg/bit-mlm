<?php

namespace common\models\user;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
use common\components\behaviors\StatusBehavior;
use common\components\UrlHelper;
use common\models\userTree\UserTree;
use common\models\userPackage\UserPackage;
use common\models\financeOperation\FinanceOperation;
use common\models\userNotification\UserNotification;
use common\models\userBalance\UserBalance;
use common\models\userPurse\UserPurse;

/**
 * User model
 *
 * @property int                $id
 * @property string             $email
 * @property string             $invite_code
 * @property string             $auth_key
 * @property string             $password_hash
 * @property string             $password_reset_token
 * @property string             $email_confirm_token
 * @property int                $status
 * @property int                $role
 * @property int                $count_visit
 * @property string             $last_visit
 * @property string             $created_at
 * @property string             $updated_at
 *
 * @property UserBalance[]      $userBalances
 * @property FinanceOperation[] $financeOperations
 * @property UserPackage[]      $userPackages
 * @property UserPurse[]        $userPurses
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_BLOCKED = 0;
    const STATUS_ON_CONFIRMATION = 1;
    const STATUS_ACTIVE = 2;

    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;

    public static $roleList
        = [
            self::ROLE_USER  => 'Пользователь',
            self::ROLE_ADMIN => 'Администратор',
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            [
                'class'         => StatusBehavior::className(),
                'statuses'      => [
                    self::STATUS_BLOCKED,
                    self::STATUS_ON_CONFIRMATION,
                    self::STATUS_ACTIVE,
                ],
                'statusLabels'  => [
                    self::STATUS_ON_CONFIRMATION => 'Не подтвержден',
                    self::STATUS_ACTIVE          => 'Активный',
                    self::STATUS_BLOCKED         => 'Заблокирован',
                ],
                'defaultStatus' => self::STATUS_ON_CONFIRMATION,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'email'                => 'Email',
            'invite_code'          => 'Инвайт-код',
            'auth_key'             => 'Auth Key',
            'password_hash'        => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email_confirm_token'  => 'Email Confirm Token',
            'status'               => 'Статус',
            'role'                 => 'Роль',
            'count_visit'          => 'Кол-во посещений',
            'last_visit'           => 'Последний визит',
            'created_at'           => 'Дата регистрации',
            'updated_at'           => 'Дата изменения',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'role', 'count_visit'], 'integer'],
            [['email', 'invite_code', 'auth_key', 'password_hash'], 'required'],
            [['last_visit', 'created_at', 'updated_at'], 'safe'],
            [['email', 'password_hash', 'password_reset_token'], 'string', 'max' => 255],
            [['invite_code'], 'string', 'max' => 9],
            [['auth_key', 'email_confirm_token'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['email_confirm_token'], 'unique'],
        ];
    }

    /**
     * @param int|string $id
     *
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @param bool   $onlyActive
     *
     * @return static|null
     */
    public static function findByEmail($email, $onlyActive = true)
    {
        $attributes = ['email' => $email];
        if ($onlyActive) {
            $attributes['status'] = self::STATUS_ACTIVE;
        }

        return static::findOne($attributes);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status'               => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     * Finds user by invite code
     *
     * @param string $inviteCode
     *
     * @return static|null
     */
    public static function findByInviteCode($inviteCode)
    {
        return static::findOne([
            'invite_code' => $inviteCode,
            //            'status'      => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateInviteCode()
    {
        do {
            $number = (string)(time() * rand(1, 10000));
            $number = substr($number, 0, 9);

            $exists = self::find()->andWhere(['invite_code' => $number])->exists();

        } while ($exists);

        $this->invite_code = $number;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function updateVisitData()
    {
        if (Yii::$app->user->isGuest) {
            return;
        }

        $user = self::findOne(Yii::$app->user->id);

        $lastVisit = Yii::$app->user->identity->last_visit;

        if (empty($lastVisit) || (date('Y-m-d', time()) !== date('Y-m-d', strtotime($lastVisit)))) {
            $user->count_visit += 1;
        }

        if ((time() - strtotime($lastVisit)) > 300) {
            $user->last_visit = new Expression('NOW()');
        }

        $user->save();
    }

    /**
     * @return string
     */
    public function getConfirmLink()
    {
        $params = [
            'user' => base64_encode($this->id),
            'hash' => $this->email_confirm_token,
        ];

        return UrlHelper::getFrontendHost() . '/auth/confirm-email?' . http_build_query($params);
    }

    /**
     * @param string $authKey
     *
     * @return static
     */
    public function getByAuthKey($authKey)
    {
        return self::findOne(['auth_key' => $authKey]);
    }

    /**
     * @return UserTree|null
     */
    public function getParentUserTree()
    {
        $model = UserTree::find()
            ->andWhere(['user_id' => $this->id])
            ->one();

        return $model->parents(1)->andWhere('level > 0')->one();
    }

    /**
     * @return static|null
     */
    public function getParentUser()
    {
        $userTree = $this->getParentUserTree();

        return !empty($userTree) ? $userTree->user : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildrenUserTreeQuery()
    {
        $userTree = UserTree::findOne(['user_id' => $this->id]);

        return $userTree->children();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserBalances()
    {
        return $this->hasMany(UserBalance::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPurses()
    {
        return $this->hasMany(UserPurse::className(), ['user_id' => 'id']);
    }

    /**
     * @return UserPurse[]
     */
    public function getActiveUserPurses()
    {
        return $this->getUserPurses()
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPackages()
    {
        return $this->hasMany(UserPackage::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserNotifications()
    {
        return $this->hasMany(UserNotification::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOperations()
    {
        return $this->hasMany(FinanceOperation::className(), ['user_id' => 'id'])
            ->alias('fo')
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE]);
    }

    /**
     * @return array
     */
    public function getAllParentUsersArray()
    {
        $user = $this;

        $array = [];

        $level = 1;
        while (true) {
            $user = $user->getParentUser();
            if (empty($user)) {
                break;
            }

            $array[$level] = $user->id;

            $level++;
        }

        return $array;
    }

    /**
     * @param int $currencyId
     *
     * @return UserBalance
     */
    public function getUserBalanceByCurrency($currencyId)
    {
        return $this->getUserBalances()
            ->andWhere(['currency_id' => $currencyId])
            ->one();
    }
}
