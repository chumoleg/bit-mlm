<?php

namespace common\models\payment;

use Yii;
use common\models\userPurse\UserPurse;

/**
 * This is the model class for table "user_payment_request".
 *
 * @property int       $id
 * @property int       $user_purse_id
 * @property int       $type
 * @property string    $payment_hash
 * @property string    $amount
 * @property int       $status
 * @property string    $created_at
 * @property string    $updated_at
 *
 * @property UserPurse $userPurse
 */
class UserPaymentRequest extends \yii\db\ActiveRecord
{
    const STATUS_NEW       = 1;
    const STATUS_WAIT      = 2;
    const STATUS_PAID      = 3;
    const STATUS_CANCELLED = 4;

    const TYPE_PAY_IN  = 1;
    const TYPE_PAY_OUT = 2;

    public static $typeList
        = [
            self::TYPE_PAY_IN  => 'Ввод средств',
            self::TYPE_PAY_OUT => 'Вывод средств',
        ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_payment_request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            [
                'class'         => \common\components\behaviors\StatusBehavior::className(),
                'statuses'      => [
                    self::STATUS_NEW,
                    self::STATUS_WAIT,
                    self::STATUS_PAID,
                    self::STATUS_CANCELLED,
                ],
                'statusLabels'  => [
                    self::STATUS_NEW       => 'Не обработан',
                    self::STATUS_WAIT      => 'На проверке',
                    self::STATUS_PAID      => 'Обработан',
                    self::STATUS_CANCELLED => 'Отклонен',
                ],
                'defaultStatus' => self::STATUS_NEW,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_purse_id', 'amount'], 'required'],
            [['type', 'user_purse_id', 'status'], 'integer'],
            [['amount'], 'number'],
            [['payment_hash', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'user_purse_id' => 'Кошелек пользователя',
            'type'          => 'Тип',
            'payment_hash'  => 'Хэш платежа',
            'amount'        => 'Сумма',
            'status'        => 'Статус',
            'created_at'    => 'Дата создания',
            'updated_at'    => 'Дата обработки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPurse()
    {
        return $this->hasOne(UserPurse::className(), ['id' => 'user_purse_id']);
    }

    /**
     * @param int         $userPurseId
     * @param int         $type
     * @param float       $amount
     * @param string|null $paymentHash
     *
     * @return static|false
     */
    public static function createRecord($userPurseId, $type, $amount, $paymentHash = null)
    {
        $model                = new self();
        $model->user_purse_id = $userPurseId;
        $model->type          = $type;
        $model->amount        = $amount;
        $model->payment_hash  = $paymentHash;

        return $model->save(false) ? $model : false;
    }

    /**
     * @return string
     */
    public function getAmountLabel()
    {
        return rtrim($this->amount, '0') . ' ' . $this->userPurse->currency->alias;
    }

    /**
     * @param int $userId
     * @param int $currencyId
     *
     * @return float
     */
    public function getAmountActiveRequests($userId, $currencyId)
    {
        return (float)self::find()
            ->alias('t')
            ->joinWith('userPurse userPurse')
            ->andWhere(['userPurse.user_id' => $userId])
            ->andWhere(['userPurse.currency_id' => $currencyId])
            ->andWhere(['IN', 't.status', [self::STATUS_NEW, self::STATUS_WAIT]])
            ->sum('t.amount');
    }
}
