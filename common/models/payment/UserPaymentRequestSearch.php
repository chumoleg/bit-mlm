<?php

namespace common\models\payment;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class UserPaymentRequestSearch extends UserPaymentRequest
{
    public $userId;
    public $userEmail;
    public $purse;
    public $currencyId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'userEmail', 'userId', 'currencyId', 'purse'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userId'     => 'Пользователь',
            'userEmail'  => 'Пользователь',
            'currencyId' => 'Валюта',
            'purse'      => 'Кошелек',
        ]);
    }

    /**
     * @param array $params
     * @param array $statuses
     *
     * @return ActiveDataProvider
     */
    public function search($params, $statuses = [])
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->joinWith(['userPurse userPurse', 'userPurse.user', 'userPurse.currency'])
            ->andFilterWhere([
                't.id'            => $this->id,
                't.amount'        => $this->amount,
                't.status'        => $this->status,
                't.type'          => $this->type,
                't.payment_hash'  => $this->payment_hash,
                'userPurse.purse' => $this->purse,
                'currency.id'     => $this->currencyId,
                'user.id'         => $this->userId,
            ]);

        if (!empty($statuses)) {
            $query->andWhere(['IN', 't.status', $statuses]);
        }

        $query->andFilterWhere(['LIKE', 'user.email', $this->userEmail]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);
        DateHelper::addQueryByDateRange($query, 't.updated_at', $this->updated_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
