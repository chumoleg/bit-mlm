<?php

namespace common\models\mining;

use Yii;
use common\components\behaviors\StatusBehavior;
use common\models\currency\Currency;
use common\models\financeOperation\FinanceOperation;

/**
 * This is the model class for table "mining_daily".
 *
 * @property int                $id
 * @property int                $currency_id
 * @property float              $power
 * @property float              $profit
 * @property string             $date
 * @property string             $created_at
 * @property string             $updated_at
 *
 * @property Currency           $currency
 * @property FinanceOperation[] $financeOperations
 */
class MiningDaily extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mining_daily';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_id', 'power', 'profit', 'date'], 'required'],
            [['currency_id'], 'integer'],
            [['power', 'profit'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'currency_id' => 'Валюта',
            'power'       => 'Суммарная мощность',
            'profit'      => 'Прибыль за день',
            'date'        => 'Дата',
            'created_at'  => 'Дата внесения',
            'updated_at'  => 'Дата изменения',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFinanceOperations()
    {
        return $this->hasMany(FinanceOperation::className(), ['mining_daily_id' => 'id'])
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE]);
    }

    /**
     * @return bool
     */
    public function hasFinanceOperations()
    {
        return $this
            ->getFinanceOperations()
            ->exists();
    }
}