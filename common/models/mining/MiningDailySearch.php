<?php

namespace common\models\mining;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class MiningDailySearch extends MiningDaily
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id'], 'safe'],
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->andFilterWhere([
                'id'          => $this->id,
                'currency_id' => $this->currency_id,
                'power'       => $this->power,
                'profit'      => $this->profit,
            ]);

        DateHelper::addQueryByDateRange($query, 'date', $this->date);
        DateHelper::addQueryByDateRange($query, 'created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
