<?php

namespace common\models\userBalance;

use Yii;
use common\models\user\User;
use common\models\currency\Currency;

/**
 * This is the model class for table "user_balance_history".
 *
 * @property int      $id
 * @property int      $user_id
 * @property string   $date
 * @property int      $currency_id
 * @property float    $balance
 * @property float    $profit
 * @property string   $created_at
 *
 * @property User     $user
 * @property Currency $currency
 */
class UserBalanceHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_balance_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date', 'currency_id', 'balance', 'profit'], 'required'],
            [['user_id', 'currency_id'], 'integer'],
            [['date', 'created_at'], 'safe'],
            [['balance', 'profit'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param string $date
     * @param int    $userId
     * @param int    $currencyId
     * @param float  $balance
     * @param float  $profit
     *
     * @return bool
     */
    public static function createRecord($date, $userId, $currencyId, $balance, $profit)
    {
        $attributes = [
            'user_id'     => $userId,
            'currency_id' => $currencyId,
            'date'        => $date,
        ];

        $model = self::findOne($attributes);
        if (empty($model)) {
            $model = new self();
            $model->setAttributes($attributes);
        }

        $model->balance = $balance;
        $model->profit  = $profit;

        return $model->save();
    }
}
