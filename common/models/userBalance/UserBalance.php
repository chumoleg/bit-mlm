<?php

namespace common\models\userBalance;

use Yii;
use common\components\behaviors\StatusBehavior;
use common\models\user\User;
use common\models\currency\Currency;
use common\models\financeOperation\FinanceOperation;

/**
 * This is the model class for table "user_balance".
 *
 * @property int      $id
 * @property int      $user_id
 * @property int      $currency_id
 * @property float    $balance
 * @property string   $created_at
 * @property string   $updated_at
 *
 * @property User     $user
 * @property Currency $currency
 */
class UserBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_balance';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'currency_id'], 'required'],
            [['user_id', 'currency_id'], 'integer'],
            [['balance'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param int         $userId
     * @param int         $currencyId
     * @param string|null $date
     */
    public function recalculate($userId, $currencyId, $date = null)
    {
        $userBalance = $this->_getModel($userId, $currencyId);
        if (empty($userBalance)) {
            return;
        }

        if (empty($date) || $date == '0000-00-00') {
            $date = date('Y-m-d');
        }

        $financeQuery = FinanceOperation::find()
            ->andWhere(['user_id' => $userId])
            ->andWhere(['currency_id' => $currencyId])
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
            ->andWhere(['<=', 'created_at', $date . ' 23:59:59']);

        $balance = (float)$financeQuery
            ->sum('IF(type_operation = ' . FinanceOperation::TYPE_OPERATION_INCOME . ', amount, -amount)');

        $userBalance->balance = $balance;
        $userBalance->save();

        $profit = (float)$financeQuery
            ->andWhere('mining_daily_id IS NOT NULL OR parent_operation_id IS NOT NULL')
            ->sum('IF(type_operation = ' . FinanceOperation::TYPE_OPERATION_INCOME . ', amount, -amount)');

        UserBalanceHistory::createRecord(
            $date, $userBalance->user_id, $userBalance->currency_id, $balance, $profit);
    }

    /**
     * @param int $userId
     * @param int $currencyId
     *
     * @return static|null
     */
    private function _getModel($userId, $currencyId)
    {
        $attributes = [
            'user_id'     => $userId,
            'currency_id' => $currencyId,
        ];

        $model = self::findOne($attributes);
        if (!empty($model)) {
            return $model;
        }

        return $this->_createNewModel($attributes);
    }

    /**
     * @param array $attributes
     *
     * @return static|null
     */
    private function _createNewModel($attributes)
    {
        $model = new self();
        $model->setAttributes($attributes);
        $model->balance = 0;

        return $model->save() ? $model : null;
    }
}
