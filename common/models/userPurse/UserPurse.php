<?php

namespace common\models\userPurse;

use Yii;
use common\components\behaviors\StatusBehavior;
use common\models\user\User;
use common\models\currency\Currency;

/**
 * This is the model class for table "user_purse".
 *
 * @property int      $id
 * @property int      $user_id
 * @property int      $currency_id
 * @property string   $purse
 * @property int      $status
 * @property string   $created_at
 *
 * @property Currency $currency
 * @property User     $user
 */
class UserPurse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_purse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'currency_id', 'purse'], 'required'],
            [['user_id', 'currency_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['purse'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            StatusBehavior::className()
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param int    $userId
     * @param int    $currencyId
     * @param string $purse
     *
     * @return bool
     */
    public static function createRecord($userId, $currencyId, $purse)
    {
        $attributes = [
            'user_id'     => (int)$userId,
            'currency_id' => (int)$currencyId,
            'status'      => StatusBehavior::STATUS_ACTIVE
        ];

        $model = self::findOne($attributes);
        if (!empty($model)) {
            if ($model->purse == $purse) {
                return true;
            }

            $model->setNotActive()->save(false);
        }

        $model = new self();
        $model->setAttributes($attributes);
        $model->purse = $purse;

        return $model->save();
    }
}
