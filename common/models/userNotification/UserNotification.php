<?php

namespace common\models\userNotification;

use Yii;
use common\models\user\User;
use common\components\behaviors\StatusBehavior;

/**
 * This is the model class for table "user_notification".
 *
 * @property int    $id
 * @property int    $user_id
 * @property int    $status
 * @property string $message
 * @property string $created_at
 *
 * @property User   $user
 */
class UserNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_notification';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\TimestampBehavior::className(),
            [
                'class'         => StatusBehavior::className(),
                'defaultStatus' => StatusBehavior::STATUS_NOT_ACTIVE,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'message'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'user_id'    => 'Пользователь',
            'status'     => 'Статус',
            'message'    => 'Сообщение',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @param int    $userId
     * @param string $message
     *
     * @return bool
     */
    public static function createNotification($userId, $message)
    {
        $model          = new self();
        $model->user_id = $userId;
        $model->message = $message;

        return $model->save();
    }
}
