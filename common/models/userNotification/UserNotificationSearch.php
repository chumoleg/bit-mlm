<?php

namespace common\models\userNotification;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;

class UserNotificationSearch extends UserNotification
{
    public $userEmail;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['id', 'userEmail'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'userEmail' => 'Пользователь',
        ]);
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = self::find()
            ->alias('t')
            ->joinWith('user')
            ->andFilterWhere([
                't.id'      => $this->id,
                't.user_id' => $this->user_id,
                't.status'  => $this->status,
            ])
            ->andFilterWhere(['LIKE', 't.message', $this->message])
            ->andFilterWhere(['LIKE', 'user.email', $this->userEmail]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
