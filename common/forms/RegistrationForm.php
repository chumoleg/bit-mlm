<?php

namespace common\forms;

use Yii;
use yii\base\Exception;
use yii\base\Model;
use common\components\MailerHelper;
use common\models\user\User;
use common\models\userTree\UserTree;

class RegistrationForm extends Model
{
    public $inviteCode;
    public $email;
    public $password;
    public $passwordRepeat;
    public $registerFirstUser = false;
    public $createdAt = null;

    public function attributeLabels()
    {
        return [
            'inviteCode'     => 'Инвайт-код',
            'email'          => 'Email',
            'password'       => 'Пароль',
            'passwordRepeat' => 'Повторите пароль',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inviteCode', 'email', 'password', 'passwordRepeat'], 'trim'],
            [['email', 'password', 'passwordRepeat'], 'required'],
            ['inviteCode', 'required', 'when' => function ($model) {
                return !$model->registerFirstUser;
            }],
            [['inviteCode'], 'string', 'max' => 9, 'min' => 9],
            [['inviteCode'], 'validateInviteCode'],
            ['email', 'email'],
            [
                'email',
                'unique',
                'targetClass' => User::className(),
                'message'     => 'Указанный email уже зарегистрирован!',
            ],
            [['email'], 'string', 'max' => 255],
            ['password', 'string', 'min' => 6],
            [
                'passwordRepeat',
                'compare',
                'compareAttribute' => 'password',
                'message'          => 'Пароли не совпадают',
            ],
        ];
    }

    public function validateInviteCode($attribute)
    {
        if ($this->registerFirstUser) {
            return true;
        }

        $userByInvite = User::findByInviteCode($this->inviteCode);
        if (empty($userByInvite)) {
            $this->addError($attribute, 'Инвайт-код недействителен!');
        }

        return true;
    }

    /**
     * @return User|false
     */
    public function registerUser()
    {
        if (!$this->validate()) {
            return false;
        }

        $userByInvite = null;
        if (!$this->registerFirstUser) {
            $userByInvite = User::findByInviteCode($this->inviteCode);
            if (empty($userByInvite)) {
                return false;
            }
        }

        $dbConnection = Yii::$app->db;
        $dbTransaction = $dbConnection->beginTransaction();

        try {
            $user = $this->_createUser();
            $user->created_at = $this->createdAt;
           
            if ($user->save()) {
                if (!UserTree::createUserTree($user, $userByInvite)) {
                    throw new \yii\db\Exception('Error creating user tree!');
                }

                $dbTransaction->commit();

                MailerHelper::sendConfirmationMail($user);

                return $user;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $dbTransaction->rollBack();

        return false;
    }

    /**
     * @return User
     */
    private function _createUser()
    {
        $user         = new User();
        $user->email  = $this->email;
        $user->status = User::STATUS_ON_CONFIRMATION;
        $user->role   = User::ROLE_USER;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailConfirmToken();
        $user->generateInviteCode();

        return $user;
    }
}