<?php

namespace common\forms;

use Yii;
use yii\base\Model;
use common\models\user\User;

class LoginForm extends Model
{
    public $email;
    public $password;

    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'trim'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if ($user && $user->validatePassword($this->password)) {
                if ($user->status == User::STATUS_ON_CONFIRMATION) {
                    $this->addError($attribute, 'Аккаунт не подтвержден');

                } elseif ($user->status == User::STATUS_BLOCKED) {
                    $this->addError($attribute, 'Аккаунт заблокирован');
                }

            } else {
                $this->addError($attribute, 'Неверный Email или пароль');
            }
        }
    }

    /**
     * @return User|null
     */
    protected function getUser()
    {
        return User::findByEmail($this->email, false);
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();

            return Yii::$app->user->login($user);

        } else {
            return false;
        }
    }
}