<?php

namespace common\forms;

use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use common\models\payment\UserPaymentRequest;
use common\models\userNotification\UserNotification;
use common\models\financeOperation\FinanceOperation;

class PaymentRequestForm extends UserPaymentRequest
{
    /**
     * @param int $id
     *
     * @return bool
     */
    public function setCancelled($id)
    {
        $model = $this->_loadModel($id);
        if ($model->status != self::STATUS_NEW) {
            return false;
        }

        $model->status = self::STATUS_CANCELLED;
        if ($model->save()) {
            UserNotification::createNotification($model->userPurse->user_id,
                $this->_getNotificationText($model) . ' отклонен');

            return true;
        }

        return false;
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function setStatusWait($id)
    {
        $model = $this->_loadModel($id);
        if ($model->status != self::STATUS_NEW) {
            return false;
        }

        if ($model->type == self::TYPE_PAY_OUT
            && $model->userPurse->currency->alias != 'ETH'
        ) {
            return $this->createFinanceOperation($model, 'without_hash');

        } else {
            $model->status = self::STATUS_WAIT;

            return $model->save();
        }
    }

    /**
     * @param int $id
     *
     * @return self|null
     * @throws NotFoundHttpException
     */
    private function _loadModel($id)
    {
        $model = self::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException('Model not found');
        }

        return $model;
    }

    /**
     * @param self $model
     *
     * @return string
     */
    private function _getNotificationText($model)
    {
        $text = $model->type == self::TYPE_PAY_IN ? 'Запрос на ввод' : 'Запрос на вывод';

        $text .= ' №' . $model->id . ' на сумму ' . $model->getAmountLabel();

        return $text;
    }

    /**
     * @param UserPaymentRequest $model
     * @param string             $paymentHash
     *
     * @return bool
     *
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function createFinanceOperation(UserPaymentRequest $model, $paymentHash)
    {
        if (!empty($model->payment_hash)) {
            return true;
        }

        $comment = $this->_getNotificationText($model);
        $userId = $model->userPurse->user_id;
        $currencyId = $model->userPurse->currency_id;

        if ($model->type == self::TYPE_PAY_OUT) {
            $operationId = FinanceOperation::createWrittenOperation(
                $userId, $currencyId, $model->amount, $comment);

        } else {
            $operationId = FinanceOperation::createIncomeOperation(
                $userId, $currencyId, $model->amount, $comment);
        }

        if (empty($operationId)) {
            throw new Exception('Ошибка при создании финансовой операции');
        }

        UserNotification::createNotification($userId, $comment . ' успешно обработан');

        $model->payment_hash = $paymentHash;
        $model->status = self::STATUS_PAID;

        return $model->save();
    }
}