<?php

namespace common\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\export\ExportMenu;

class LoadCsvGridView extends GridView
{
    public  $options       = ['class' => 'grid-view'];
    public  $layout        = "{pager}\n{summary}\n{items}";
    private $_columnsArray = [];

    public function init()
    {
        $this->_columnsArray = $this->columns;

        parent::init();
    }

    /**
     * Renders the data models for the grid view.
     */
    public function renderItems()
    {
        $widget      = '';
        $countModels = $this->dataProvider->getTotalCount();
        if (!empty($countModels)) {
            $widget = ExportMenu::widget([
                'clearBuffers'     => true,
                'dataProvider'     => $this->dataProvider,
                'columns'          => $this->_columnsArray,
                'target'           => ExportMenu::TARGET_SELF,
                'showConfirmAlert' => false,
                'folder'           => Yii::$app->getRuntimePath() . '/export',
                'batchSize'        => 200,
                'asDropdown'       => false,
                'filename'         => $this->id,
                'exportConfig'     => [
                    ExportMenu::FORMAT_CSV     => [
                        'options'     => [
                            'tag' => 'div',
                        ],
                        'linkOptions' => [
                            'class' => 'btn btn-success grid-load-csv-button',
                        ],
                        'iconOptions' => ['class' => ''],
                    ],
                    ExportMenu::FORMAT_TEXT    => false,
                    ExportMenu::FORMAT_PDF     => false,
                    ExportMenu::FORMAT_EXCEL   => false,
                    ExportMenu::FORMAT_EXCEL_X => false,
                    ExportMenu::FORMAT_HTML    => false,
                ],
            ]);
        }

        return Html::tag('div', $widget . parent::renderItems(), ['class' => 'my-grid-table-block']);
    }
}