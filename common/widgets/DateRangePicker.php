<?php

namespace common\widgets;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

class DateRangePicker extends \kartik\daterange\DateRangePicker
{

    public $containerTemplate
        = <<< HTML
        <div class="form-control kv-drp-dropdown">
            <span class="pull-left">
                <span class="range-value">{value}</span>
            </span>
            {input}
        </div>
HTML;

    public $addon = true;
    public $convertFormat  = true;
    public $presetDropdown = true;
    public $hideInput      = true;

    public $options = ['class' => 'daterangepicker'];

    public function run()
    {
        $this->pluginOptions = ArrayHelper::merge([
            'locale' => [
                'format'      => 'd.m.Y',
                'cancelLabel' => 'Очистить',
            ],
        ], $this->pluginOptions);

        parent::run();
    }

    /**
     * Initializes the pluginOptions range list
     */
    protected function initRange()
    {
        if (isset($dummyValidation)) {
            /** @noinspection PhpUnusedLocalVariableInspection */
            $msg = Yii::t('kvdrp', 'Select Date Range');
        }

        if ($this->presetDropdown) {
            $this->initRangeExpr           = true;
            $this->pluginOptions['ranges'] = [
                Yii::t('kvdrp', "Today")                      => [
                    "moment().startOf('day')",
                    "moment()",
                ],
                Yii::t('kvdrp', "Yesterday")                  => [
                    "moment().startOf('day').subtract(1,'days')",
                    "moment().endOf('day').subtract(1,'days')",
                ],
                Yii::t('kvdrp', "Last {n} Days", ['n' => 7])  => [
                    "moment().startOf('day').subtract(6, 'days')",
                    "moment()",
                ],
                Yii::t('kvdrp', "Last {n} Days", ['n' => 30]) => [
                    "moment().startOf('day').subtract(29, 'days')",
                    "moment()",
                ],
                'Последние 3 месяца'                          => [
                    "moment().subtract(2, 'month').startOf('month')",
                    "moment().endOf('month')",
                ],
                Yii::t('kvdrp', "Last Month")                 => [
                    "moment().subtract(1, 'month').startOf('month')",
                    "moment().subtract(1, 'month').endOf('month')",
                ],
                Yii::t('kvdrp', "This Month")                 => [
                    "moment().startOf('month')",
                    "moment().endOf('month')",
                ],
                'Этот год'                                    => [
                    "moment().startOf('year')",
                    "moment()",
                ],
            ];
        }
        if (!$this->initRangeExpr
            || empty($this->pluginOptions['ranges'])
            || !is_array($this->pluginOptions['ranges'])
        ) {
            return;
        }

        $range = [];
        foreach ($this->pluginOptions['ranges'] as $key => $value) {
            if (!is_array($value) || empty($value[0]) || empty($value[1])) {
                throw new InvalidConfigException(
                    "Invalid settings for pluginOptions['ranges']. Each range value must be a two element array."
                );
            }

            $range[$key] = [static::parseJsExpr($value[0]), static::parseJsExpr($value[1])];
        }

        $this->pluginOptions['ranges'] = $range;
    }
}