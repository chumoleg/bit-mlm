<?php

namespace common\widgets;

use Yii;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Alert extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes
        = [
            'error'   => 'alert-danger',
            'danger'  => 'alert-danger',
            'success' => 'alert-success',
            'info'    => 'alert-info',
            'warning' => 'alert-warning',
        ];
    /**
     * @var array the options for rendering the close button tag.
     */
    public $closeButton = [];


    public function init()
    {
        parent::init();

        $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        foreach ($this->alertTypes as $type => $className) {
            $flashMessages = (array)Yii::$app->session->getFlash($type);
            if (empty($flashMessages)) {
                continue;
            }

            foreach ($flashMessages as $i => $flashMessage) {
                /* initialize css class for each alert box */
                $this->options['class'] = $className . $appendCss;

                /* assign unique id to each alert box */
                $this->options['id'] = $this->getId() . '-' . $type . '-' . $i;

                echo \yii\bootstrap\Alert::widget([
                    'body'        => $flashMessage,
                    'closeButton' => $this->closeButton,
                    'options'     => $this->options,
                ]);
            }
        }
    }
}