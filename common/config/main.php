<?php

require(__DIR__ . DIRECTORY_SEPARATOR . 'container.php');

return [
    'aliases'        => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath'     => dirname(dirname(__DIR__)) . '/vendor',
    'sourceLanguage' => 'en-US',
    'language'       => 'ru-RU',
    'name'           => 'Resimico Cloud Mining',
    'bootstrap'      => ['log'],
    'modules'        => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'components'     => [
        'consoleRunner' => [
            'class' => 'toriphes\console\Runner',
        ],
        'assetManager'  => [
            'linkAssets'      => true,
            'appendTimestamp' => true,
        ],
        'user'          => [
            'class'           => 'common\components\User',
            'identityClass'   => 'common\models\user\User',
            'enableAutoLogin' => true,
        ],
        'curl'          => [
            'class' => 'linslin\yii2\curl\Curl',
        ],
        'cache'         => [
            'class'        => 'yii\caching\MemCache',
            'useMemcached' => true,
        ],
        'urlManager'    => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
        ],
//        'mailer'       => [
//            'class'            => 'yii\swiftmailer\Mailer',
//            'viewPath'         => '@common/mail',
//            'useFileTransport' => false,
//        ],
        'mailer'        => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => 'smtp.gmail.com',
                'username'   => 'bit.mlm.service',
                'password'   => '1SGy5SW88Ow9',
                'port'       => '587',
                'encryption' => 'tls',
            ],
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'logVars' => [],
                    'levels'  => ['error', 'warning'],
                ],
            ],
        ],
    ],
];
