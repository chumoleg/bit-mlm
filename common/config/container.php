<?php

Yii::$container->set('yii\data\Sort', [
    'defaultOrder' => [
        'id' => SORT_DESC,
    ],
]);

Yii::$container->set('yii\data\Pagination', [
    'pageSize' => 20,
]);

Yii::$container->set('yii\widgets\Pjax', [
    'options' => [
        'class' => 'pjax-wrapper',
    ],
    'timeout' => 10000,
]);

Yii::$container->set('yii\jui\DatePicker', [
    'dateFormat'    => 'yyyy.MM.dd',
    'clientOptions' => [
        'changeMonth' => true,
        'changeYear'  => true,
        'yearRange'   => '2018:' . date('Y'),
    ],
    'options'       => ['class' => 'form-control'],
]);

Yii::$container->set('kartik\date\DatePickerAsset', [
    'jsOptions' => [
        'position' => \yii\web\View::POS_HEAD,
    ],
]);

Yii::$container->set('kartik\daterange\DateRangePicker', [
    'presetDropdown' => true,
    'hideInput'      => true,
    'pluginOptions'  => [
        'format' => 'DD.MM.YYYY',
    ],
    'options'        => ['class' => 'daterangepicker'],
]);

Yii::$container->set('kartik\date\DatePicker', [
    'pickerButton'  => false,
    'type'          => \kartik\date\DatePicker::TYPE_COMPONENT_APPEND,
    'pluginOptions' => [
        'autoclose'   => true,
        'format'      => 'dd.mm.yyyy',
        'changeMonth' => true,
        'changeYear'  => true,
    ],
]);

Yii::$container->set('yii\widgets\Breadcrumbs', [
    'tag'                => 'ol',
    'options'            => [
        'class' => 'c-breadcrumb u-mb-medium',
    ],
    'itemTemplate'       => ' <li class="c-breadcrumb__item">{link}</li>',
    'activeItemTemplate' => ' <li class="c-breadcrumb__item is-active">{link}</li>',
]);

$pagerOptions = [
    'options'              => ['class' => 'c-pagination__list'],
    'linkContainerOptions' => [
        'class' => 'c-pagination__item',
    ],
    'linkOptions'          => [
        'tag'   => 'a',
        'class' => 'c-pagination__link',
    ],
    'maxButtonCount'       => 8,
    'prevPageLabel'        => '<',
    'nextPageLabel'        => '>',
    'firstPageLabel'       => '<<',
    'lastPageLabel'        => '>>',
    'activePageCssClass'   => 'is-active',
];

Yii::$container->set('yii\grid\GridView', [
    'layout'           => "
        <div class='row'>
            <div class='col-md-4 align-bottom'>{summary}</div>
            <div class='col-md-8'><nav class='c-pagination u-justify-end'>{pager}</nav></div>
        </div>
        {items}",
    'pager'            => $pagerOptions,
    'tableOptions'     => [
        'class' => 'c-table',
    ],
    'headerRowOptions' => [
        'class' => 'c-table__head--slim',
    ],
    'filterRowOptions' => [
        'class' => 'c-table__head--slim',
    ],
    'rowOptions'       => [
        'class' => 'c-table__row',
    ],
    'dataColumnClass'  => '\common\components\grid\DataColumn',
]);

Yii::$container->set('kartik\grid\GridView', [
    'export'           => false,
    'resizableColumns' => false,
    'footerRowOptions' => ['class' => 'info text-bold'],
    'layout'           => "{pager}\n{summary}\n{items}",
    'pager'            => $pagerOptions,
]);

\Yii::$container->set('yii\widgets\ListView', [
    'layout' => "
        <div class='row'>{items}</div>
        <div class='row'>
            <div class='col-md-12'>
                <nav class='c-pagination u-justify-center'>{pager}</nav>
            </div>
        </div>",
    'pager'  => $pagerOptions,
]);

Yii::$container->set('kartik\grid\Select2', [
    'size' => 'sm',
]);

Yii::$container->set('kartik\select2\Select2', [
    'pluginOptions' => [
        'closeOnSelect'    => false,
        'shouldFocusInput' => false,
    ],
]);

Yii::$container->set('yii\i18n\Formatter', [
    'dateFormat'        => 'php:d.m.Y',
    'datetimeFormat'    => 'php:d.m.Y H:i:s',
    'thousandSeparator' => ' ',
    'decimalSeparator'  => '.',
    'timeZone'          => 'UTC',
    'nullDisplay'       => '',
]);

Yii::$container->set('yii\bootstrap\Tabs', [
    'options'       => [
        'class' => 'c-tabs__list c-tabs__list--splitted nav nav-tabs'
    ],
    'headerOptions' => [
        'class' => 'c-tabs__item'
    ],
    'linkOptions'   => [
        'class' => 'c-tabs__link'
    ],
]);
