<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';

    public $css
        = [
            'static/css/front.min.css',
            'static/css/main.min.css',
            'static/css/site.css',
        ];

    public $js
        = [
//            'static/js/main.min.js',
            'static/js/main-2.js',
        ];

    public $depends
        = [
            'yii\web\YiiAsset',
            'yii\bootstrap\BootstrapPluginAsset',
        ];
}
