<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\components\DateHelper;
use common\models\financeOperation\FinanceOperation;
use common\models\userPackage\UserPackage;

class UserTreeSearch extends \common\models\userTree\UserTreeSearch
{
    public $countPackages;
    public $profitData;

//    public function rules()
//    {
//        return ArrayHelper::merge(parent::rules(), [
//            [['countPackages'], 'safe'],
//        ]);
//    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'countPackages' => 'Кол-во купленных пакетов',
            'profitData'    => 'Прибыль от партнера',
        ]);
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $userTreeInnerQuery = Yii::$app->user->identity
            ->getChildrenUserTreeQuery()
            ->select(['user_id']);

        $this->load($params);

        $innerOperationQuery = FinanceOperation::find()
            ->alias('fo')
            ->select(['GROUP_CONCAT(DISTINCT currency.id)'])
            ->joinWith(['parentOperation pfo', 'currency currency'])
            ->andWhere(['fo.user_id' => Yii::$app->user->id])
            ->andWhere('pfo.user_id = t.user_id');

        $query = self::find()
            ->alias('t')
            ->joinWith(['user'])
            ->select([
                't.*',
                'userEmail'     => 'user.email',
                'countPackages' => '(SELECT COUNT(id) FROM ' . UserPackage::tableName()
                    . ' WHERE user_id = t.user_id AND status = 1)',
                'profitData'    => $innerOperationQuery,
            ])
            ->andWhere('t.level > 0')
            ->andWhere(['IN', 't.user_id', $userTreeInnerQuery])
            ->andFilterWhere([
                't.id'      => $this->id,
                't.user_id' => $this->user_id,
                't.level'   => $this->level,
            ])
            ->andFilterWhere(['LIKE', 'user.email', $this->userEmail]);

        DateHelper::addQueryByDateRange($query, 't.created_at', $this->created_at);

        return new ActiveDataProvider([
            'query' => $query,
            //            'pagination' => [
            //                'pageSize' => 1
            //            ]
        ]);
    }

    public function getProfitAmountLabel($currencyId)
    {
        return FinanceOperation::find()
            ->alias('fo')
            ->select(['CONCAT_WS(" ", SUM(fo.amount), currency.alias)'])
            ->joinWith(['parentOperation pfo', 'currency currency'])
            ->andWhere(['fo.user_id' => Yii::$app->user->id])
            ->andWhere(['pfo.user_id' => $this->user_id])
            ->andWhere(['currency.id' => $currencyId])
            ->createCommand()
            ->queryScalar();
    }
}