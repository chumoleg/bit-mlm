<?php

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;

class PackageSearch extends \common\models\package\PackageSearch
{
    public function search($params)
    {
        $query = self::find()
            ->andWhere( ['status' => \common\components\behaviors\StatusBehavior::STATUS_ACTIVE]);

        $userPackageIds = array_keys(Yii::$app->user->identity->getUserPackageCounts());
        if (!empty($userPackageIds)) {
            $query->orWhere(['IN', 'id', $userPackageIds]);
        }

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 4
            ]
        ]);
    }
}