<?php

namespace frontend\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\components\behaviors\StatusBehavior;
use common\components\foreignSystems\BitFinex;
use common\models\financeOperation\FinanceOperation;
use common\models\userNotification\UserNotification;
use common\models\userTree\UserTree;

class User extends \common\models\user\User
{
    /**
     * @return array
     */
    public function getUserPackageCounts()
    {
        $data = $this->getUserPackages()
            ->select(['package_id', 'count' => 'COUNT(id)'])
            ->andWhere(['status' => StatusBehavior::STATUS_ACTIVE])
            ->groupBy('package_id')
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($data, 'package_id', 'count');
    }

    /**
     * @return array
     */
    public function getDashboardBalances()
    {
        $data = $this->getUserBalances()
            ->select(['currency' => 'currency.alias', 'amount' => 'SUM(balance)'])
            ->joinWith(['currency'])
            ->groupBy('currency_id')
            ->createCommand()
            ->queryAll();

        return $this->_getDashboardFinanceArray($data);
    }

    /**
     * @return array
     */
    public function getDashboardIncomingByPartners()
    {
        $data = $this->getFinanceOperations()
            ->select(['currency' => 'currency.alias', 'amount' => 'SUM(amount)'])
            ->joinWith(['currency'])
            ->andWhere(['type_operation' => FinanceOperation::TYPE_OPERATION_INCOME])
            ->andWhere('parent_operation_id IS NOT NULL')
            ->groupBy('currency_id')
            ->createCommand()
            ->queryAll();

        return $this->_getDashboardFinanceArray($data);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function _getDashboardFinanceArray($data)
    {
        if (empty($data)) {
            return [];
        }

        $array = [];
        foreach ($data as $item) {
            $amount        = $item['amount'];
            $currencyAlias = $item['currency'];

            $equalUsdValue = BitFinex::getEqualUsd($amount, $currencyAlias);

            $array[] = [
                'amount'   => rtrim($amount, '0') . ' ' . $currencyAlias,
                'equalUsd' => round($equalUsdValue, 2) . 'USD',
            ];
        }

        return $array;
    }

    /**
     * @return array
     */
    public function getDashboardIncomingByPackages()
    {
        $data = $this->getFinanceOperations()
            ->select(['currency' => 'currency.alias', 'amount' => 'SUM(amount)'])
            ->joinWith(['currency'])
            ->andWhere('mining_daily_id IS NOT NULL')
            ->andWhere(['type_operation' => FinanceOperation::TYPE_OPERATION_INCOME])
            ->groupBy('currency_id')
            ->createCommand()
            ->queryAll();

        return $this->_getDashboardFinanceArray($data);
    }

    /**
     * @return UserNotification[]
     */
    public function getLastNotifications()
    {
        return $this->getUserNotifications()
            ->orderBy(['id' => SORT_DESC])
            ->limit(10)
            ->all();
    }

    /**
     * @return FinanceOperation[]
     */
    public function getLastFinanceOperations()
    {
        return $this->getFinanceOperations()
            ->orderBy(['id' => SORT_DESC])
            ->limit(10)
            ->all();
    }

    /**
     * @return UserTree[]
     */
    public function getLastPartners()
    {
        return $this
            ->getChildrenUserTreeQuery()
            ->joinWith('user')
            ->limit(10)
            ->all();
    }

    public function getInviteLink()
    {
        return \yii\helpers\Url::base('http') . '/auth/registration?i=' . $this->invite_code;
    }

    /**
     * @return int
     */
    public function getCountNewNotifications()
    {
        return (int)$this->getUserNotifications()
            ->andWhere(['status' => StatusBehavior::STATUS_NOT_ACTIVE])
            ->count();
    }
}
