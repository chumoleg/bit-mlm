<?php

namespace frontend\controllers;

use Yii;
use frontend\components\BaseController;
use frontend\models\PackageSearch;

class DashboardController extends BaseController
{
    public $showTitle = false;

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function getPackageDataProvider()
    {
        $searchModel  = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return [$searchModel, $dataProvider];
    }
}