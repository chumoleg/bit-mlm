<?php

namespace frontend\controllers;

use Yii;
use frontend\components\BaseController;
use frontend\models\UserTreeSearch;

class PartnerController extends BaseController
{
    public $showTitle = false;

    public function actionIndex()
    {
        $searchModel  = new UserTreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
