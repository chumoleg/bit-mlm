<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use frontend\components\BaseController;
use frontend\forms\UserPurseForm;

class ProfileController extends BaseController
{
    public $layout = 'profile';
    public $showTitle = false;

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPurse()
    {
        $model = new UserPurseForm();

        $model->purseList = ArrayHelper::map(Yii::$app->user->identity->getActiveUserPurses(), 'currency_id', 'purse');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveModel()) {
                Yii::$app->session->setFlash('success', 'Изменения сохранены успешно');

                return $this->refresh();
            }
        }

        return $this->render('purse', [
            'model' => $model
        ]);
    }
}