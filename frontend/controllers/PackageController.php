<?php

namespace frontend\controllers;

use Yii;
use yii\base\UserException;
use common\components\behaviors\StatusBehavior;
use common\models\package\Package;
use frontend\components\BaseController;
use frontend\forms\BuyPackageForm;
use yii\helpers\ArrayHelper;

class PackageController extends BaseController
{
    public function actionModal($id)
    {
        $model = $this->_loadModel($id);

        return $this->renderAjax('modal', [
            'model' => $model,
        ]);
    }

    public function actionBuy()
    {
        $id = Yii::$app->request->post('id');
        if (!Yii::$app->request->isAjax || empty($id)) {
            throw new UserException('Access denied', 403);
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        try {
            $model = $this->_loadModel($id);
            if ($model->status != StatusBehavior::STATUS_ACTIVE) {
                throw new UserException('Пакет не может быть куплен');
            }

            $form            = new BuyPackageForm();
            $form->userId    = Yii::$app->user->id;
            $form->packageId = $model->id;

            if ($form->buyPackage()) {
                $userPackageCounts = Yii::$app->user->identity->getUserPackageCounts();

                return [
                    'status'    => 'success',
                    'message'   => 'Пакет успешно куплен!',
                    'countHtml' => $this->renderPartial('_count-bought',
                        ['count' => ArrayHelper::getValue($userPackageCounts, $model->id)]),
                ];
            }

            $errorMessage = 'Ошибка при покупке пакета';
            if (!empty($form->errors)) {
                $errorMessage = $form->getFirstError('userId');
            }

        } catch (UserException $e) {
            $errorMessage = $e->getMessage();
        }

        return [
            'status'  => 'error',
            'message' => $errorMessage,
        ];
    }

    /**
     * @param int $id
     *
     * @return null|Package
     * @throws UserException
     */
    private function _loadModel($id)
    {
        $model = Package::findOne($id);
        if (empty($model)) {
            throw new UserException('Пакет не найден', 404);
        }

        return $model;
    }
}