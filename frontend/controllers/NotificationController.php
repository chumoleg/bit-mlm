<?php

namespace frontend\controllers;

use Yii;
use common\components\behaviors\StatusBehavior;
use common\models\userNotification\UserNotification;
use common\models\userNotification\UserNotificationSearch;
use frontend\components\BaseController;

class NotificationController extends BaseController
{
    public function actionIndex()
    {
        $searchModel          = new UserNotificationSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $dataProvider         = $searchModel->search(Yii::$app->request->queryParams);

        UserNotification::updateAll(['status' => StatusBehavior::STATUS_ACTIVE],
            'user_id = ' . Yii::$app->user->id . ' AND status = ' . StatusBehavior::STATUS_NOT_ACTIVE);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}