<?php

namespace frontend\controllers;

use Yii;
use common\components\controllers\BaseController;
use common\forms\LoginForm;
use common\forms\RegistrationForm;
use common\models\user\User;
use frontend\forms\PasswordResetRequestForm;
use frontend\forms\ResetPasswordForm;
use frontend\forms\ConfirmEmailForm;

class AuthController extends BaseController
{
    public $layout = 'blank';
    public $showFooter = false;

    public function beforeAction($action)
    {
        if (!Yii::$app->user->isGuest && !in_array($action->id, ['logout', 'switch-identity'])) {
            $this->goHome();

            return false;
        }

        if (!parent::beforeAction($action)) {
            return false;
        }

        return true;
    }

    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/');
        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/');
    }

    public function actionRegistration($i = null)
    {
        $model             = new RegistrationForm();
        $model->inviteCode = $i;

        if ($model->load(Yii::$app->request->post()) && $user = $model->registerUser()) {
            Yii::$app->session->setFlash('success', 'Регистрация прошла успешно. На ваш Email отправлена ссылка для активации аккаунта.');

            return $this->redirect('/');
        }

        return $this->render('registration', compact('model'));
    }

    public function actionForgotPassword()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success',
                    'Данные для восстановления пароля отправлены на указанный Email');

                return $this->redirect('/');

            } else {
                Yii::$app->session->setFlash('error', 'Восстановление пароля невозможно');
            }
        }

        return $this->render('forgot-password', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     *
     * @return mixed
     */
    public function actionResetPassword($token)
    {
        $model = new ResetPasswordForm($token);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->redirect(['login']);
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    public function actionConfirmEmail($user, $hash)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model        = new ConfirmEmailForm();
        $model->user  = base64_decode($user);
        $model->token = $hash;

        if ($user = $model->confirmEmail()) {
            Yii::$app->session->setFlash('success', 'Email успешно подтвержден');

            Yii::$app->user->login($user);

            return $this->goHome();
        }

        Yii::$app->session->setFlash('error', 'Ошибка при подтверждении Email');

        return $this->redirect('/');
    }

    public function actionSwitchIdentity($authKey)
    {
        $user = (new User())->getByAuthKey($authKey);

        if (Yii::$app->user->getIsGuest()) {
            Yii::$app->user->login($user);

        } else {
            Yii::$app->user->logout();
            Yii::$app->user->switchIdentity($user);
        }

        return $this->redirect(Yii::$app->getHomeUrl());
    }
}