<?php

namespace frontend\controllers;

use Yii;
use common\components\behaviors\StatusBehavior;
use common\models\payment\UserPaymentRequestSearch;
use common\models\financeOperation\FinanceOperationSearch;
use frontend\components\BaseController;
use frontend\forms\PayOutForm;

class FinanceController extends BaseController
{
    public $layout = 'finance';
    public $showTitle = false;

    public function actionIndex()
    {
        $searchModel = new FinanceOperationSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->status = StatusBehavior::STATUS_ACTIVE;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOperation()
    {
        $searchModel = new FinanceOperationSearch();
        $searchModel->user_id = Yii::$app->user->id;
        $searchModel->status = StatusBehavior::STATUS_ACTIVE;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('operation', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayIn()
    {
        $searchModel = new UserPaymentRequestSearch();
        $searchModel->userId = Yii::$app->user->id;
        $searchModel->type = PayOutForm::TYPE_PAY_IN;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('pay-in', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayOut()
    {
        $searchModel = new UserPaymentRequestSearch();
        $searchModel->userId = Yii::$app->user->id;
        $searchModel->type = PayOutForm::TYPE_PAY_OUT;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('pay-out', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPayOutCreate()
    {
        $model = new PayOutForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->createPayment()) {
                Yii::$app->session->setFlash('success', 'Запрос будет обработан нашими специалистами в ближайшее время');

                return $this->redirect(['payment']);
            }
        }

        return $this->render('pay-out-form', [
            'model' => $model,
        ]);
    }
}