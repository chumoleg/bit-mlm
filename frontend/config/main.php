<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['frontend\components\ApplicationParams'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute'        => '/auth/login',
    'components'          => [
        'request'      => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user'         => [
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'identityClass'  => 'frontend\models\User',
        ],
        'session'      => [
            'name' => 'advanced-frontend',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params'              => $params,
];
