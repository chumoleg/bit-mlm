<?php

$explodedHost = explode('.', $_SERVER['HTTP_HOST']);
$baseUrl = 'http://' . $explodedHost[count($explodedHost) - 2] . '.' . $explodedHost[count($explodedHost) - 1];

return [
    'adminEmail' => 'admin@example.com',
    'baseUrl'    => $baseUrl
];
