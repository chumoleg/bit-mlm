$(document).ready(function () {
    onCancelDatePicker();

    function onCancelDatePicker() {
        $('.kv-drp-container').on('cancel.daterangepicker', function () {
            $(this).find('.range-value').text('');
            $(this).find('.daterangepicker').val('').trigger('change');
        });
    }

    $(document).on('ready pjax:success', function () {
        onCancelDatePicker();
    });

    var $document = $(document),
        $sidebar = $(".js-page-sidebar"), // page sidebar itself
        $sidebarToggle = $(".c-sidebar-toggle"), // sidebar toggle icon
        $sidebarToggleContainer = $(".c-navbar"), // component that contains sidebar toggle icon
        $sidebarItem = $(".c-sidebar__item"),
        $sidebarSubMenu = $(".c-sidebar__submenu");

    $sidebarToggleContainer.on('click', function (e) {
        var $target = $(e.target);
        if ($target.closest($sidebarToggle).length) {
            $sidebar.addClass('is-visible');
            return false;
        }
    });

    // Bootstrap collapse.js plugin is used for handling sidebar submenu.
    $sidebarSubMenu.on('show.bs.collapse', function () {
        $(this).parent($sidebarItem).addClass('is-open');
    });

    $sidebarSubMenu.on('hide.bs.collapse', function () {
        $(this).parent($sidebarItem).removeClass('is-open');
    });

    $document.on('click', function (e) {
        var $target = $(e.target);
        if (!$target.closest($sidebar).length) {
            $sidebar.removeClass('is-visible');
        }
    });

    $(document).on('click', '.package-modal-btn', function () {
        var modal = $('#packageModal');

        modal.removeData('bs.modal');
        modal.modal({remote: '/package/modal?id=' + $(this).data('id')});
        modal.modal('show');
    });

    $(document).on('click', '.buy-package', function () {
        var packId = $(this).data('id');
        var block = $(this).closest('.modal-footer');

        $.post('/package/buy', {id: packId}, function (answer) {
            if (answer.status == 'error') {
                block.find('.u-text-danger').html(answer.message).removeClass('u-hidden');

            } else {
                block.find('.u-text-success').html(answer.message).removeClass('u-hidden');
                $('.count-bought-block-' + packId).html(answer.countHtml);
            }

            setTimeout(function () {
                block.find('.u-text-success').addClass('u-hidden');
            }, 2000);
        }, 'json');
    });

    $('#packageModal').on('hide.bs.modal', function () {
        $(this).removeData('bs.modal');
        $(this).find('.modal-content').html('');
    });
});

function windowPreloaderShow() {
    var bodyH = $(window).height() / 2 - 90;
    $('body').css('opacity', 0.6);
    $('#windowPreLoader').css('padding-top', bodyH + 'px').show();
}

function windowPreloaderHide() {
    setTimeout(function () {
        var obj = $('body #windowPreLoader');
        obj.fadeOut('slow', function () {
            obj.hide();
            $('body').css('opacity', '');
        });
    }, 100);
}