<?php

namespace frontend\forms;

use Yii;
use yii\base\Model;
use common\models\user\User;
use common\models\userNotification\UserNotification;

class ConfirmEmailForm extends Model
{
    public $user;
    public $token;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'token'], 'trim'],
            [['user', 'token'], 'required'],
            ['user', 'integer'],
            ['token', 'string', 'max' => 32, 'min' => 32],
        ];
    }

    /**
     * @return User|false
     */
    public function confirmEmail()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = User::findOne($this->user);
        if (empty($user)) {
            $this->addError('user', 'Пользователь не найден');

            return false;
        }

        if ($user->status != User::STATUS_ON_CONFIRMATION || $user->email_confirm_token != $this->token) {
            $this->addError('user', 'Email уже подтвержден');

            return false;
        }

        $user->status = User::STATUS_ACTIVE;
        $user->email_confirm_token = null;
        if ($user->save(false)) {
            UserNotification::createNotification($user->id, 'Аккаунт активирован');

            $parentUser = $user->getParentUser();
            if (!empty($parentUser)) {
                $parentNotification = 'Зарегистрирован новый партнер ("' . $user->email . '")';
                UserNotification::createNotification($parentUser->id, $parentNotification);
            }
        }

        return $user;
    }
}