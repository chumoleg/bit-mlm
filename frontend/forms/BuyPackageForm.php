<?php

namespace frontend\forms;

use common\models\payment\UserPaymentRequest;
use Yii;
use yii\base\Model;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use common\models\user\User;
use common\models\package\Package;
use common\models\userPackage\UserPackage;
use common\models\financeOperation\FinanceOperation;
use common\models\userNotification\UserNotification;

class BuyPackageForm extends Model
{
    public $packageId;
    public $userId;

    /**
     * @var User
     */
    private $_user;

    /**
     * @var Package
     */
    private $_package;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['packageId', 'userId'], 'trim'],
            [['packageId', 'userId'], 'required'],
            [['userId'], 'validateUserBalance'],
        ];
    }

    public function beforeValidate()
    {
        $this->_user = User::findOne($this->userId);
        $this->_package = Package::findOne($this->packageId);

        return parent::beforeValidate();
    }

    public function validateUserBalance($attribute)
    {
        $currencyId = $this->_package->currency_id;

        $userBalance = $this->_user->getUserBalanceByCurrency($currencyId);

        $balance = !empty($userBalance) ? $userBalance->balance : 0;
        $onRequestPaymentAmount = (new UserPaymentRequest)->getAmountActiveRequests($this->_user->id, $currencyId);

        $availableBalance = $balance - $onRequestPaymentAmount;

        if ($availableBalance < $this->_package->price) {
            $this->addError($attribute,
                'Недостаточно средств для покупки данного пакета. Доступно: ' . $availableBalance
                . '. <a href="/finance/pay-in">Пополните баланс!</a>');
        }
    }

    /**
     * @return bool
     */
    public function buyPackage()
    {
        if (!$this->validate()) {
            return false;
        }

        $dbTransaction = Yii::$app->db->beginTransaction();

        try {
            if (!UserPackage::createRecord($this->userId, $this->_package)) {
                throw new Exception('Error creating user package');
            }

            $this->_createFianceOperations();

            $notification = 'Куплен пакет мощности "' . $this->_package->name . '"';
            UserNotification::createNotification($this->_user->id, $notification);

            $dbTransaction->commit();

            return true;

        } catch (Exception $e) {
        }

        $dbTransaction->rollBack();

        return false;
    }

    /**
     * @throws Exception
     */
    private function _createFianceOperations()
    {
        $currencyId = $this->_package->currency_id;

        // проводим операцию списания стоимости пакета
        $mainOperationId = FinanceOperation::createWrittenOperation(
            $this->_user->id,
            $currencyId,
            $this->_package->price,
            'Покупка пакета "' . $this->_package->name . '"'
        );

        if (empty($mainOperationId)) {
            throw new Exception('Ошибка при списании средств');
        }

        $parentUserIds = $this->_user->getAllParentUsersArray();

        $percentArray = $this->_package->getPercentArray();

        foreach ($percentArray as $k => $percent) {
            $userId = ArrayHelper::getValue($parentUserIds, $k);
            if (empty($userId)) {
                continue;
            }

            $amount = round($this->_package->price * $percent / 100, 8);

            // проводим операцию поступления процентов за партнера
            $comment = 'Начисление средств от партнера "' . $this->_user->email . '" за покупку пакета "'
                . $this->_package->name . '"';

            if (!FinanceOperation::createIncomeOperation($userId, $currencyId, $amount, $comment, $mainOperationId)) {
                throw new Exception('Ошибка при начислении средств партнерам');
            }
        }
    }
}
