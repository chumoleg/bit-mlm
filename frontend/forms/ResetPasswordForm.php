<?php

namespace frontend\forms;

use Yii;
use yii\base\Model;
use yii\base\UserException;
use common\models\user\User;

class ResetPasswordForm extends Model
{
    public $password;
    public $passwordRepeat;

    /**
     * @var User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array  $config name-value pairs that will be used to initialize the object properties
     *
     * @throws UserException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new UserException('Токен не указан', 400);
        }

        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new UserException('Некорректный токен. Возможно он уже использован', 403);
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'passwordRepeat'], 'trim'],
            [['password', 'passwordRepeat'], 'required'],
            [
                'passwordRepeat',
                'compare',
                'compareAttribute' => 'password',
                'message'          => 'Пароли не совпадают',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password'       => 'Пароль',
            'passwordRepeat' => 'Повторите пароль',
        ];
    }

    /**
     * Resets password.
     *
     * @return bool if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
