<?php

namespace frontend\forms;

use Yii;
use yii\base\Model;
use common\models\user\User;

class ChangePasswordForm extends Model
{
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'trim'],
            [['email', 'password'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * Resets password.
     *
     * @param bool $passwordHashed
     *
     * @return bool if password was reset.
     */
    public function resetPassword($passwordHashed = false)
    {
        $user = User::findByEmail($this->email);
        if (empty($user)) {
            return false;
        }

        if ($passwordHashed) {
            $user->password_hash = $this->password;
        } else {
            $user->setPassword($this->password);
        }

        $user->removePasswordResetToken();

        return $user->save(false);
    }
}
