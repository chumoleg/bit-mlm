<?php

namespace frontend\forms;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\payment\UserPaymentRequest;

class PayOutForm extends UserPaymentRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'user_purse_id'], 'trim'],
            [['amount', 'user_purse_id'], 'required'],
            [['amount'], 'number', 'min' => 0.00000001],
            [['amount'], 'validateUserBalance'],
        ];
    }

    public function validateUserBalance($attribute)
    {
        if (empty($this->amount) || $this->amount < 0) {
            $this->addError('amount', 'Укажите сумму вывода');

            return true;
        }

        $userId = $this->userPurse->user_id;
        $currencyId = $this->userPurse->currency_id;

        $userBalance = Yii::$app->user->identity->getUserBalanceByCurrency($currencyId);
        $balance = !empty($userBalance) ? $userBalance->balance : 0;
        $onRequestPaymentAmount = (new UserPaymentRequest)->getAmountActiveRequests($userId, $currencyId);

        $availableBalance = $balance - $onRequestPaymentAmount;

        if ($availableBalance < $this->amount) {
            $this->addError($attribute, 'Недостаточно средств для вывода. Доступно: ' . $availableBalance);
        }

        return true;
    }

    /**
     * @return static|false
     */
    public function createPayment()
    {
        if (!$this->validate()) {
            return false;
        }

        return UserPaymentRequest::createRecord($this->user_purse_id, self::TYPE_PAY_OUT, $this->amount);
    }

    /**
     * @return array
     */
    public function getUserPurseList()
    {
        $currencyIds = ArrayHelper::getColumn(Yii::$app->user->identity->userBalances, 'currency_id');
        if (empty($currencyIds)) {
            return [];
        }

        $userPurses = Yii::$app->user->identity->getActiveUserPurses();

        $array = [];
        foreach ($userPurses as $userPurse) {
            if (!in_array($userPurse->currency_id, $currencyIds)) {
                continue;
            }

            $array[$userPurse->id] = '(' . $userPurse->currency->alias . ') ' . $userPurse->purse;
        }

        return $array;
    }
}
