<?php

namespace frontend\forms;

use Yii;
use yii\base\Model;
use common\models\currency\Currency;
use common\models\userPurse\UserPurse;

class UserPurseForm extends Model
{
    public $purseList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purseList'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function saveModel()
    {
        if (!$this->validate()) {
            return false;
        }

        $currencyList = Currency::getList();

        foreach ($this->purseList as $currencyId => $purse) {
            if (!isset($currencyList[$currencyId])) {
                continue;
            }

            UserPurse::createRecord(Yii::$app->user->id, $currencyId, trim($purse));
        }

        return true;
    }
}
