<?php

use yii\helpers\Html;

$this->title = $name;

?>

<div class="c-error">
    <header class="c-error__head">
        <span class="c-error__icon">
            <i class="fa fa-chain-broken"></i>
        </span>
        <h1 class="c-error__title">
            <?= !empty($exception->statusCode) ? $exception->statusCode : $exception->getCode(); ?>
        </h1>
    </header>

    <div class="c-error__body">
        <h2 class="c-error__description"><?= nl2br(Html::encode($message)); ?></h2>
    </div>

    <footer class="c-error__footer">
        <a class="c-error__footer-link u-left" href="/">
            <i class="fa fa-long-arrow-left"></i>Вернуться на главную
        </a>
    </footer>
</div>