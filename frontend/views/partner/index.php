<?php

use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Мои партнеры';

$userPackageCounts = Yii::$app->user->identity->getUserPackageCounts();

?>

<div class="row">
    <div class="col-md-12 u-p-small u-mb-small text-right">
        <?php if (!empty($userPackageCounts)) : ?>
            <h2>Инвайт-код для приглашения партнеров: <strong><?= Yii::$app->user->identity->invite_code; ?></strong>
            </h2>
            <h5>Ссылка для приглашения партнеров: <strong><?= Yii::$app->user->identity->getInviteLink(); ?></strong>
            </h5>
        <?php else : ?>
            <h2 class="u-text-danger">Для того, чтобы приглашать партнеров, необходимо приобрести хотя бы один
                пакет.</h2>
        <?php endif; ?>
    </div>
</div>

<div class="c-card u-p-medium u-mb-small">

    <?php
    Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'userEmail',
            'level',
            [
                'attribute' => 'countPackages',
            ],
            [
                'class'     => \common\components\grid\AmountColumn::className(),
                'attribute' => 'profitData',
                'format'    => 'raw',
                'value'     => function ($data) {
                    if (empty($data->profitData)) {
                        return 0;
                    }

                    $currencies = explode(',', $data->profitData);

                    $array = [];
                    foreach ($currencies as $currencyId) {
                        $array[] = $data->getProfitAmountLabel($currencyId);
                    }

                    return implode('<br />', $array);
                }
            ],
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>