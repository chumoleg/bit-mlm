<?php

use yii\grid\GridView;

$this->title = 'Финансовые операции';

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'rowOptions'   => function ($model, $index, $widget, $grid) {
        $cssClass = 'u-color-success';
        if ($model->type_operation == \common\models\financeOperation\FinanceOperation::TYPE_OPERATION_WRITTEN) {
            $cssClass = 'u-color-danger';
        }

        return ['class' => $cssClass];
    },
    'columns'      => [
        [
            'attribute' => 'id',
            'options'   => ['width' => '100'],
        ],
        [
            'class' => \common\components\grid\CurrencyColumn::className(),
        ],
        [
            'attribute' => 'type_operation',
            'filter'    => \common\models\financeOperation\FinanceOperation::$typeOperationList,
            'value'     => function ($data) {
                return $data->getTypeOperationLabel();
            },
        ],
        [
            'attribute' => 'amount',
            'value'     => function ($data) {
                return $data->getAmountLabel();
            },
        ],
        [
            'attribute' => 'comment',
            'contentOptions' => [
                'class' => 'wrappedColumn'
            ]
        ],
        [
            'class' => \common\components\grid\DateColumn::className(),
        ],
    ],
]);