<?php

use yii\grid\GridView;

$this->title = 'Вывод средств';

?>

    <div class="row">
        <div class="col-12">
            <a href="/finance/pay-out-create" class="c-btn c-btn--info">Создать запрос на вывод</a>
        </div>
    </div>

    <div>&nbsp;</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        [
            'attribute' => 'id',
            'options'   => ['width' => '100'],
        ],
        [
            'class'     => \common\components\grid\AmountColumn::className(),
            'attribute' => 'amount',
        ],
        [
            'attribute' => 'currencyId',
            'filter'    => \common\models\currency\Currency::getList(),
            'value'     => 'userPurse.currency.alias'
        ],
        [
            'attribute' => 'purse',
            'value'     => 'userPurse.purse'
        ],
        [
            'class' => \common\components\grid\StatusColumn::className(),
        ],
        [
            'class' => \common\components\grid\DateColumn::className(),
        ],
        [
            'class'     => \common\components\grid\DateColumn::className(),
            'attribute' => 'updated_at',
        ],
    ],
]);
?>