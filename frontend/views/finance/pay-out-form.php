<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\forms\PayOutForm;

/** @var PayOutForm $model */

$this->title = 'Создание запроса на вывод средств';

$purseList = $model->getUserPurseList();
?>

<legend><?= $this->title; ?></legend>
<div>&nbsp;</div>

<?php if (empty($purseList)) : ?>
    <h2 class="u-text-danger">
        Для того, чтобы проводить финансовые операции, необходимо <a href="/profile/purse">настроить кошельки</a>.
    </h2>

    <?php return; ?>
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>
<div class="row">
    <div class="col-md-4">
        <?= $form->field($model, 'amount')->textInput() ?>
        <?= $form->field($model, 'user_purse_id')->dropDownList($purseList) ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Отправить запрос', ['class' => 'c-btn c-btn--default']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
