<?php

use yii\grid\GridView;

$this->title = 'Ввод средств';

$currency = \common\models\currency\Currency::getEth();
?>

<div class="row">
    <div class="col-12">
        <h3 class="u-text-danger u-mb-medium">
            На данный момент пополнение доступно только через кошелек <?= $currency->alias; ?><br />
            Минимальная сумма пополнения 0.00000001 <?= $currency->alias; ?>
        </h3>

        <div class="u-text-large">
            Для того чтобы пополнить баланс, переведите необходимую сумму на кошелек:
        </div>
        <h1 class="u-mb-small">
            <?= $currency->receiving_purse; ?>
        </h1>

        * Средства поступят на баланс в течение 30 минут.
    </div>
</div>

<div>&nbsp;</div>
<legend>Операции ввода средств</legend>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        [
            'attribute' => 'id',
            'options'   => ['width' => '100'],
        ],
        [
            'class'     => \common\components\grid\AmountColumn::className(),
            'attribute' => 'amount',
        ],
        [
            'attribute' => 'currencyId',
            'filter'    => \common\models\currency\Currency::getList(),
            'value'     => 'userPurse.currency.alias'
        ],
        [
            'attribute'      => 'purse',
            'value'          => 'userPurse.purse',
            'contentOptions' => ['class' => 'wrappedColumn'],
        ],
        [
            'attribute'      => 'payment_hash',
            'contentOptions' => ['class' => 'wrappedColumn'],
        ],
        [
            'class' => \common\components\grid\StatusColumn::className(),
        ],
        [
            'class' => \common\components\grid\DateColumn::className(),
        ],
    ],
]);
?>