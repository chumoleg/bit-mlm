<?php

$this->title = 'Аналитика';

$chartHelper = new \common\components\ChartHelper();
?>

<div class="row">
    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render('@common/views/chart', $chartHelper->getUserBalanceHistory(Yii::$app->user->id)); ?>
        </div>
    </div>
    <div class="col-6">
        <div class="c-graph-card">
            <?= $this->render('@common/views/chart', $chartHelper->getUserProfitData(Yii::$app->user->id)); ?>
        </div>
    </div>
</div>