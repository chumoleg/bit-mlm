<?php
/** @var \common\models\package\Package $model */
?>

<div class="c-gallery-card u-p-small u-mb-small">
    <div class="c-gallery-card__header u-text-bold">
        <h4><?= $model->name; ?> (<?= $model->profitCurrency->alias; ?>)</h4>

        <?php if ($model->is_hit) : ?>
            <p class="u-text-success">ХИТ</p>
        <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-5">
            <div class="u-mt-small">
                Мощность:<br/>
                <strong><?= $model->getPowerLabel(); ?></strong>
            </div>
            <div class="u-mt-small">
                Стоимость:<br/>
                <strong><?= $model->getPriceLabel(); ?></strong>
            </div>

            <div class="u-mt-small count-bought-block-<?= $model->id; ?>">
            <?= $this->render('@app/views/package/_count-bought',
                ['count' => \yii\helpers\ArrayHelper::getValue($userPackageCounts, $model->id, 0)]); ?>
            </div>
        </div>

        <div class="col-7 text-center">
            <a data-id="<?= $model->id; ?>" class="package-modal-btn">
                <img src="<?= $model->getImgSrc(); ?>" alt="<?= $model->name; ?>" class="c-card--responsive">
            </a>
        </div>
    </div>
    <div class="u-mt-small">&nbsp;</div>

    <div class="row">
        <div class="col-12">
            <button type="button" class="c-btn c-btn--success package-modal-btn" data-id="<?= $model->id; ?>">
                Подробнее
            </button>
        </div>
    </div>
</div>