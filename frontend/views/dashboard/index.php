<?php

use yii\widgets\Pjax;

$userModel = Yii::$app->user->identity;

list($packageSearch, $packageDataProvider) = $this->context->getPackageDataProvider();
$userPackageCounts = $userModel->getUserPackageCounts();

$balancesData = $userModel->getDashboardBalances();
$incomingByPackages = $userModel->getDashboardIncomingByPackages();
$incomingByPartners = $userModel->getDashboardIncomingByPartners();

//$lastNotifications     = $userModel->getLastNotifications();
$lastPartners = $userModel->getLastPartners();
$lastFinanceOperations = $userModel->getLastFinanceOperations();
?>

<div class="row">
    <?php
    $financeData = [
        [
            'data'       => $balancesData,
            'title'      => 'Текущий баланс',
            'colorClass' => 'c-state-card__icon--warning',
            'iconClass'  => 'fa-pie-chart',
        ],
        [
            'data'       => $incomingByPackages,
            'title'      => 'Доход с моих пакетов',
            'colorClass' => 'c-state-card__icon--success',
            'iconClass'  => 'fa-line-chart',
        ],
        [
            'data'       => $incomingByPartners,
            'title'      => 'Доход от партнеров',
            'colorClass' => 'c-state-card__icon--fancy',
            'iconClass'  => 'fa-user',
        ],
    ];

    foreach ($financeData as $viewParams) {
        echo $this->render('_finance-item', $viewParams);
    }
    ?>
</div>

<div class="row">
    <div class="col-12">
        <div class="package-list-view">
            <?php
            Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false]);
            echo \yii\widgets\ListView::widget([
                'dataProvider' => $packageDataProvider,
                'itemView'     => '_package-list-item',
                'viewParams'   => ['userPackageCounts' => $userPackageCounts],
                'itemOptions'  => [
                    'class' => 'col-lg-3 col-sm-6',
                ],
            ]);
            Pjax::end();
            ?>
        </div>
    </div>
</div><!-- // .row -->

<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="c-card u-mb-medium">
            <div class="c-card__head">
                <h5 class="c-card__title">Последние финансовые операции</h5>
                <div class="c-card__meta">
                    <a href="/finance/index">Показать все</a>
                </div>
            </div>

            <table class="c-table u-border-zero">
                <?php foreach ($lastFinanceOperations as $obj) : ?>

                    <?php
                    $cssClass = 'u-color-success';
                    if ($obj->type_operation == \common\models\financeOperation\FinanceOperation::TYPE_OPERATION_WRITTEN) {
                        $cssClass = 'u-color-danger';
                    }
                    ?>

                    <tr class="c-table__row <?= $cssClass; ?>">
                        <td class="c-table__cell" width="30%"><?= $obj->getAmountLabel(); ?></td>
                        <td class="c-table__cell" width="40%"><?= $obj->comment; ?></td>
                        <td class="c-table__cell u-text-right" width="30%">
                            <span class="u-text-mute"><?= $obj->created_at; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>

    <div class="col-md-6 col-xs-12">
        <div class="c-card u-mb-medium">
            <div class="c-card__head">
                <h5 class="c-card__title">Последние зарегистрированные партнеры</h5>
                <div class="c-card__meta">
                    <a href="/partner/index">Показать все</a>
                </div>
            </div>

            <table class="c-table u-border-zero">
                <?php foreach ($lastPartners as $obj) : ?>
                    <tr class="c-table__row">
                        <td class="c-table__cell" width="70%"><?= $obj->user->email; ?></td>
                        <td class="c-table__cell u-text-right" width="30%">
                            <span class="u-text-mute"><?= $obj->created_at; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </div>
</div><!-- // .row -->

<div class="modal fade" tabindex="-1" role="dialog" id="packageModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content"></div>
    </div>
</div>