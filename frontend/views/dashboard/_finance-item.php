<div class="col-md-4 col-sm-6">
    <div class="c-gallery-card u-p-small side-finance-block">
        <div class="row">
            <div class="col-3 text-center u-mb-none">
                <div class="c-state-card__icon <?= $colorClass; ?>">
                    <i class="fa <?= $iconClass; ?>"></i>
                </div>
            </div>

            <div class="col-9">
                <p class="c-state-card__meta"><?= $title; ?></p>

                <?php if (!empty($data)) : ?>
                    <?php foreach ($data as $item) : ?>
                        <div class="u-text-bold"><?= $item['amount'] . ' (~' . $item['equalUsd'] . ')'; ?></div>
                    <?php endforeach; ?>
                <?php else : ?>
                    Пока ничего нет!
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>