
<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="c-tabs">
    <?= \yii\bootstrap\Tabs::widget(
        [
            'id'    => 'financeTabs',
            'items' => [
                [
                    'label'  => 'Аналитика',
                    'url'    => ['finance/index'],
                    'linkOptions' => [
                        'class' => strstr(Yii::$app->request->url, 'finance/index') ? 'c-tabs__link active' : 'c-tabs__link'
                    ]
                ],
                [
                    'label'  => 'Финансовые операции',
                    'url'    => ['finance/operation'],
                    'linkOptions' => [
                        'class' => strstr(Yii::$app->request->url, 'finance/operation') ? 'c-tabs__link active' : 'c-tabs__link'
                    ]
                ],
                [
                    'label'  => 'Ввод средств',
                    'url'    => ['finance/pay-in'],
                    'linkOptions' => [
                        'class' => strstr(Yii::$app->request->url, 'finance/pay-in') ? 'c-tabs__link active' : 'c-tabs__link'
                    ]
                ],
                [
                    'label'  => 'Вывод средств',
                    'url'    => ['finance/pay-out'],
                    'linkOptions' => [
                        'class' => strstr(Yii::$app->request->url, 'finance/pay-out') ? 'c-tabs__link active' : 'c-tabs__link'
                    ]
                ],
            ],
        ]
    );
    ?>

    <div class="c-tabs__content tab-content" id="nav-tabContent">
        <div class="c-tabs__pane active">
            <div class="row">
                <div class="col-12">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>