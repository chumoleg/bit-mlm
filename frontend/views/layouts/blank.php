<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;

$this->title = Yii::$app->name . ' | ' . $this->title;

\frontend\assets\AppAsset::register($this);

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= Yii::$app->name; ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="/static/apple-touch-icon.png?v=2">
    <link rel="shortcut icon" href="/static/favicon.ico?v=2" type="image/x-icon">

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<div class="wrap">
    <?= $content; ?>
</div>

<?php if ($this->context->showFooter) : ?>
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 u-text-right">
<!--                    <div class="u-inline-block u-text-center">-->
<!--                        <a class="" href="/">-->
<!--                            <img src="/static/img/Resimico_Ru_Black.png" width="60" alt="--><?php //echo Yii::$app->name; ?><!--">-->
<!--                        </a>-->
<!--                    </div>-->
<!---->
<!--                    <div class="u-inline-block u-ml-large u-mr-large u-text-left">-->
<!--                        <ul>-->
<!--                            <li><a class="c-login__footer-link u-text-small" href="/site/about">О компании</a></li>-->
<!--                            <li><a class="c-login__footer-link u-text-small" href="/site/rule">Правила</a></li>-->
<!--                            <li><a class="c-login__footer-link u-text-small" href="/site/contact">Контакты</a></li>-->
<!--                        </ul>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </footer>
<?php endif; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
