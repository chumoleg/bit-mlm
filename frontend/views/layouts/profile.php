
<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<div class="c-tabs">
    <?= \yii\bootstrap\Tabs::widget(
        [
            'id'    => 'profileTabs',
            'items' => [
//                [
//                    'label'  => 'Персональные настройки',
//                    'url'    => ['profile/index'],
//                    'linkOptions' => [
//                        'class' => strstr(Yii::$app->request->url, 'profile/index') ? 'c-tabs__link active' : 'c-tabs__link'
//                    ]
//                ],
                [
                    'label'  => 'Кошельки',
                    'url'    => ['profile/purse'],
                    'linkOptions' => [
                        'class' => strstr(Yii::$app->request->url, 'profile/purse') ? 'c-tabs__link active' : 'c-tabs__link'
                    ]
                ],
            ],
        ]
    );
    ?>

    <div class="c-tabs__content tab-content" id="nav-tabContent">
        <div class="c-tabs__pane active">
            <div class="row">
                <div class="col-12">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>