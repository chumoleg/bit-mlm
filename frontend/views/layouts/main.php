<?php $this->beginContent('@app/views/layouts/blank.php'); ?>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" title="<?= Yii::$app->name; ?>">
                    <img src="/static/img/Resimico_Ru_Black.png" alt="<?= Yii::$app->name; ?>">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php
                $requestUrl = Yii::$app->request->url;

                $menuItems = [
                    [
                        'label'  => 'Партнёры',
                        'url'    => '/partner/index',
                        'active' => (bool)strstr($requestUrl, '/partner/'),
                    ],
                    [
                        'label'  => 'Финансовые операции',
                        'url'    => '/finance/index',
                        'active' => (bool)strstr($requestUrl, '/finance/'),
                    ],
                ];
                ?>

                <ul class="nav navbar-nav">
                    <?php foreach ($menuItems as $menuItem) : ?>
                        <li class="<?= $menuItem['active'] ? 'active' : ''; ?>">
                            <a href="<?= $menuItem['url']; ?>" title="<?= $menuItem['label']; ?>">
                                <?= $menuItem['label']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php $countNotifications = Yii::$app->user->identity->getCountNewNotifications(); ?>

                    <li class="<?= (bool)strstr($requestUrl, '/notification/') ? 'active' : ''; ?>">
                        <a class="c-notification" href="/notification/index" title="Уведомления">
                            <span class="c-notification__icon">
                                <i class="fa fa-user-o"></i>
                            </span>
                            <?php if ($countNotifications > 0) : ?>
                                <span class="c-notification__number"><?= $countNotifications; ?></span>
                            <?php endif; ?>
                        </a>
                    </li>
                    <li class="<?= (bool)strstr($requestUrl, '/profile/') ? 'active' : ''; ?>">
                        <a class="c-notification" href="/profile/purse" title="Настройки">
                            <span class="c-notification__icon">
                                <i class="fa fa-cogs"></i>
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="/auth/logout">Выход (<?= Yii::$app->user->identity->email; ?>)</a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container-fluid">
        <?php if ($this->context->showTitle) : ?>
            <div class="c-toolbar u-justify-space-between u-mb-medium">
                <h4 class="c-toolbar__title u-mr-auto"><?= $this->title; ?></h4>
            </div>
        <?php endif; ?>

        <?= common\widgets\Alert::widget(); ?>
        <?= $content; ?>
    </div>

<?php $this->endContent(); ?>