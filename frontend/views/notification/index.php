<?php

use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Уведомления/сообщения';
?>

<div class="c-card u-p-medium u-mb-small">
    <?php
    Pjax::begin(['enablePushState' => false, 'enableReplaceState' => false]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'id',
                'options'   => ['width' => '100'],
            ],
            'message',
            [
                'class' => \common\components\grid\DateColumn::className(),
            ],
        ],
    ]);
    Pjax::end();
    ?>
</div>