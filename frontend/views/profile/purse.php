<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Кошельки';

?>

<div class="row">
    <div class="col-lg-4">
        <?php $form = ActiveForm::begin(['enableClientValidation' => false]); ?>

        <?php foreach (\common\models\currency\Currency::getList() as $currencyId => $currencyAlias) : ?>
            <?= $currencyAlias; ?><br/>
            <?= $form->field($model, 'purseList[' . $currencyId . ']')->label(false)->textInput() ?>
        <?php endforeach; ?>

        <div>&nbsp;</div>
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'c-btn c-btn--default']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>