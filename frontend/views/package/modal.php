<?php

/** @var \common\models\package\Package $model */

$userPackageCounts = Yii::$app->user->identity->getUserPackageCounts();

?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">
        <?= $model->name; ?> (<?= $model->profitCurrency->alias; ?>)
    </h4>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-md-6">
            <div class="u-mt-small">
                Мощность:<br/>
                <strong><?= $model->getPowerLabel(); ?></strong>
            </div>
            <div class="u-mt-small">
                Стоимость:<br/>
                <strong><?= $model->getPriceLabel(); ?></strong>
            </div>

            <div class="u-mt-small count-bought-block-<?= $model->id; ?>">
                <?= $this->render('_count-bought',
                    ['count' => \yii\helpers\ArrayHelper::getValue($userPackageCounts, $model->id, 0)]); ?>
            </div>

            <div class="u-mt-small">
                <?= $model->description; ?>
            </div>
        </div>
        <div class="col-md-6 u-text-right">
            <img src="<?= $model->getImgSrc(); ?>" alt="<?= $model->name; ?>" class="c-card--responsive">
        </div>
    </div>
</div>

<div class="modal-footer">
    <div class="row">
        <div class="col-md-6 u-text-bold">
            <div class="u-text-success u-hidden u-text-large"></div>
            <div class="u-text-danger u-hidden"></div>
        </div>

        <div class="col-md-6 u-text-right">
            <?php if ($model->status == \common\components\behaviors\StatusBehavior::STATUS_ACTIVE) : ?>
                <a href="#" class="buy-package c-btn c-btn--large c-btn--info" data-id="<?= $model->id; ?>">
                    Купить этот пакет!
                </a>
            <?php else : ?>
                <div class="u-text-danger u-text-bold u-text-large">Пакет в архиве</div>
            <?php endif; ?>
        </div>
    </div>
</div>