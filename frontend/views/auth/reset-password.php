<?php

use kartik\form\ActiveForm;

$this->title = 'Сброс пароля';

?>

<div class="c-login">
    <header class="c-login__head u-text-center">
        <span class="c-login__icon">
            <i class="u-h2 fa fa-lock"></i>
        </span>

        <div class="row u-justify-center">
            <div class="col-9">
                <h1 class="c-login__title"><?= $this->title; ?></h1>
            </div>
        </div>
    </header>

    <?php $form = ActiveForm::begin([
        'id'          => 'reset-password-form',
        'options'     => [
            'class' => 'c-login__content',
        ],
        'fieldConfig' => [
            'addClass' => 'form-control c-input',
        ],
    ]); ?>

    <div class="c-field u-mb-small">
        <?= $form->field($model, 'password', ['labelOptions' => ['class' => 'c-field__label']])->passwordInput([
            'placeholder' => 'Numbers, Letters...',
        ]); ?>
    </div>

    <div class="c-field u-mb-small">
        <?= $form->field($model, 'passwordRepeat', ['labelOptions' => ['class' => 'c-field__label']])->passwordInput([
            'placeholder' => 'Numbers, Letters...',
        ]); ?>
    </div>

    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Сохранить новый пароль</button>

    <?php ActiveForm::end(); ?>
</div>