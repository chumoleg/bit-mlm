<?php

use kartik\form\ActiveForm;

$this->title = 'Восстановление пароля';

?>

<div class="c-login">
    <header class="c-login__head u-text-center">
        <a class="c-login__brand" href="/">
            <img src="/static/img/Resimico_EN_Black.png" alt="<?= Yii::$app->name; ?>">
        </a>

        <div class="row u-justify-center">
            <div class="col-9">
                <h1 class="c-login__title"><?= $this->title; ?></h1>
                <p class="u-h6 u-text-mute">Введите свой адрес электронной почты, чтобы получить инструкции по восстановлению пароля</p>
            </div>
        </div>
    </header>

    <?php $form = ActiveForm::begin([
        'id'          => 'forgot-password-form',
        'options'     => [
            'class' => 'c-login__content',
        ],
        'fieldConfig' => [
            'addClass' => 'form-control c-input',
        ],
    ]); ?>

    <div class="c-field u-mb-small">
        <?= $form->field($model, 'email', ['labelOptions' => ['class' => 'c-field__label']])->textInput([
            'placeholder' => 'user@example.com',
        ]); ?>
    </div>

    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Отправить инструкции</button>

    <?php ActiveForm::end(); ?>

    <footer class="c-login__footer">
        <a class="c-login__footer-link u-left" href="/auth/registration">У меня нет аккаунта!</a>

        <a class="c-error__footer-link u-float-right" href="<?= Yii::$app->params['baseUrl']; ?>">
            <i class="fa fa-long-arrow-left"></i>Вернуться на главную
        </a>
    </footer>
</div>