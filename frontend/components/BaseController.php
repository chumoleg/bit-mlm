<?php

namespace frontend\components;

use Yii;
use common\models\user\User;

class BaseController extends \common\components\controllers\BaseController
{
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->goHome();

            return false;
        }

        if (!parent::beforeAction($action)) {
            return false;
        }

        (new User())->updateVisitData();

        return true;
    }
}