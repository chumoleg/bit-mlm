<?php

namespace frontend\components;

use Yii;
use yii\helpers\ArrayHelper;

class CookieHelper
{
    /**
     * @param array $cookieParams
     */
    public static function add($cookieParams)
    {
        $cookieParams = ArrayHelper::merge($cookieParams, [
            'domain' => self::_getDomain(),
        ]);

        Yii::$app->response->cookies->add(new \yii\web\Cookie($cookieParams));
    }

    /**
     * @return string
     */
    private static function _getDomain()
    {
        return ArrayHelper::getValue(Yii::$app->user->identityCookie, 'domain');
    }

    /**
     * @param string $cookieName
     *
     * @return mixed|null
     */
    public static function getValue($cookieName)
    {
        return Yii::$app->request->cookies->getValue($cookieName);
    }

    /**
     * @param string $cookieName
     */
    public static function remove($cookieName)
    {
        $cookie = self::get($cookieName);
        if (empty($cookie)) {
            return;
        }

        $cookie->domain = self::_getDomain();

        Yii::$app->response->cookies->remove($cookie);
    }

    /**
     * @param string $cookieName
     *
     * @return \yii\web\Cookie
     */
    public static function get($cookieName)
    {
        return Yii::$app->request->cookies->get($cookieName);
    }
}