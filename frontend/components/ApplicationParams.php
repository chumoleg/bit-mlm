<?php

namespace frontend\components;

use Yii;
use yii\base\BootstrapInterface;
use yii\helpers\Url;

class ApplicationParams implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        if (Yii::$app->user->isGuest) {
            return;
        }

        $homeUrl = Url::to('/dashboard/index');
        Yii::$app->setHomeUrl($homeUrl);
        Yii::$app->defaultRoute = $homeUrl;

//        Yii::$app->urlManager->addRules(['/' => $homeUrl], false);
    }
}