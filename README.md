**Краткое описание проекта:**

Данный проект позволяет пользователю покупать пакеты мощностей для майнинга криптовалют. 

Используется партнерская структура продаж, каждый пользователь регистрируется только по инвайт-коду (привязывается к партнеру).

В дальнейшем при покупке какого-либо пакета, прользователь получает прибыль в криптовалюте.

Партнеры уровнями выше получают процент от прибыли своего реферала

Пользователь при входе на сайт попадает на лендинг с описанием проекта, контактами, входом и регистрацией.

После регистрации и подтверждения email и/или входа пользователь попадает в личный кабинет.

В кабинете несколько разделов: партнеры, финансовые операции (ввод/вывод средств), покупка пакетов, уведомления, страница статистики, персональные настройки.

После ввода средств на баланс, пользователь покупает выбранный пакет мощностей.

Начисления прибыли по всем купленным пакетам производится по крону каждый день.

**Техническая часть:**

на сервере используется php7.2, mysql5.7

Проект разработан с использованием фреймворка Yii2 (https://www.yiiframework.com/).

Для подключения самого фреймворка и сторонних библиотек, используется composer.

Используется шаблон Yii Advanced. Он позволяет разделить проект на подпроекты с возможностью отдельно их конфигурировать и разделять на независимые части с отдельными точками входа, разделением прав и т.п.

Проект разворачивается по инструкции фреймворка: https://github.com/yiisoft/yii2-app-advanced/blob/master/docs/guide/start-installation.md

Структура БД полностью прописана в миграциях и разворачивается командой в консоли: php ./yii migrate

Для запуска кронов используется сторонний модуль https://github.com/gofmanaa/yii2-crontask

Для хранения дерева пользователей (связь с рефералами) используется дерево каталогов NESTED SETS

Для отслеживание поступлений средств и текущих курсов криптовалют, по API подключены сторонние системы: https://bitfinex.com и http://etherscan.io

система хранения версий: GIT

**Структура проекта:**
```
/backend    - Администраторская часть системы
/frontend   - Публичная часть, личный кабинет пользователя
/console    - Команды, используемые в консоли, кроны
/common     - Общие для всех подпроектов конфиги, компоненты, модели, вьюхи и т.д.

Директории в подпроектах: 
/config     - Конфигурационные файлы, настройка подключений и т.д.
/components - Общие классы, хелперы, сюда же в вынес общие контроллеры
/forms      - Классы для подготовки, валидации и сохранения данных. основаны на моделях
/web        - index.php, css, js, картинки
остальные директории по схеме mvc
```